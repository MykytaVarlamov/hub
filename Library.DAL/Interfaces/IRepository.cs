﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Library.DAL.Models;

namespace Library.DAL.Interfaces
{
  public interface IRepository<T> where T : class
  {
    void CreateAsyncAsTask(T item);
    Task<IEnumerable<T>> GetAllAsyncAsTask();
    Task<T> GetAsyncAsTask(int id);

    //T GetById(int id);
    void DeleteAsync(int id);
    void Update(T item);
    IEnumerable<T> GetAll();
  }
}
