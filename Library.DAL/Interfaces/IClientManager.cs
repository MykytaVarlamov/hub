﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.DAL.Models;

namespace Library.DAL.Interfaces
{
  /// <summary>
  /// интерфейс содержит один метод для создания нового профиля пользователя
  /// </summary>
  public interface IClientManager : IDisposable
  {
    void Create(ClientProfile item);
  }
}
