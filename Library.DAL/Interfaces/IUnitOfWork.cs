﻿using Library.DAL.Models;
using System;
using System.Threading.Tasks;
using Library.DAL.Identity;

namespace Library.DAL.Interfaces
{
  public interface IUnitOfWork : IDisposable
  {
    IRepository<Article> Articles { get; }
    IRepository<Review> Reviews { get; }
    IRepository<Form> Forms { get; }
    IRepository<Tag> Tags { get; }
    IRepository<ArticleTag> ArticleTags { get; }
    /// <summary>
    /// Менеджер пользователей
    /// </summary>
    ApplicationUserManager UserManager { get; }
    /// <summary>
    /// Менеджер ролей
    /// </summary>
    ApplicationRoleManager RoleManager { get; }
    /// <summary>
    /// Репозиторий пользователей
    /// </summary>
    IClientManager ClientManager { get; }
    Task SaveAsync();


    void Save();
  }
}
