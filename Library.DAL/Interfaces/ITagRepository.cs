﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.DAL.Models;

namespace Library.DAL.Interfaces
{
  interface ITagRepository
  {
    IEnumerable<Tag> GetAll();
  }
}
