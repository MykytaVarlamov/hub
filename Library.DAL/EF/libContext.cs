﻿using Library.DAL.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Library.DAL.EF
{
  public class LibContext : IdentityDbContext<ApplicationUser>
  {
    static LibContext()
    {
      Database.SetInitializer<LibContext>(new LibDbInitializer());
    }
    public LibContext(string connectionString) : base(connectionString) { }

    public LibContext() : base("HubConnection")
    {
      
    }

    //protected override void OnModelCreating(DbModelBuilder modelBuilder)
    //{
    //  //modelBuilder.Entity<IdentityUserLogin>().HasKey<string>(l => l.UserId);
    //  //modelBuilder.Entity<IdentityRole>().HasKey<string>(r => r.Id);
    //  //modelBuilder.Entity<IdentityUserRole>().HasKey(r => new { r.RoleId, r.UserId });

    //  //modelBuilder.Entity<Article>()
    //  //  .HasRequired(a => a.ApplicationUser)
    //  //  .WithMany()
    //  //  .WillCascadeOnDelete(false);
    //  //base.OnModelCreating(modelBuilder);
    //  //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
    //  //modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
    //  //modelBuilder.Entity<ApplicationUser>()
    //  //        .HasRequired(u => u.Articles)
    //  //        .WithMany()
    //  //        .WillCascadeOnDelete(false);

    //}

    public DbSet<ClientProfile> ClientProfiles { get; set; }

    //public DbSet<ApplicationUser> ApplicationUsers { get; set; }
    public DbSet<Article> Articles { get; set; }
    public DbSet<Tag> Tags { get; set; }
    public DbSet<ArticleTag> ArticleTags { get; set; }
    public DbSet<Review> Reviews { get; set; }
    public DbSet<Form> Forms { get; set; }

    //public static LibContext Create()
    //{
    //    return new LibContext();
    //}

  }
}