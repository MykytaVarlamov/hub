﻿using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using Library.DAL.Identity;
using Library.DAL.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using static Library.DAL.Enum;

namespace Library.DAL.EF
{
  public class LibDbInitializer : DropCreateDatabaseIfModelChanges<LibContext>
  {
    protected override void Seed(LibContext context)
    {
      try
      {
        var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
        var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

        var role1 = new IdentityRole { Name = "admin" };
        var role2 = new IdentityRole { Name = "user" };
        roleManager.Create(role1);
        roleManager.Create(role2);

        var admin = new ApplicationUser { Email = "admin@gmail.com", UserName = "admin@gmail.com" };
        var adminPassword = "password";
        var result0 = userManager.Create(admin, adminPassword);

        var user1 = new ApplicationUser { Email = "varlaminik37@gmail.com", UserName = "varlaminik37@gmail.com" };
        var user1Password = "password";
        var result1 = userManager.Create(user1, user1Password);

        var user2 = new ApplicationUser { Email = "detkabob@gmail.com", UserName = "detkabob@gmail.com" };
        var user2Password = "password";
        var result2 = userManager.Create(user2, user2Password);

        // добавляем для пользователя роль
        if (result0.Succeeded && result1.Succeeded && result2.Succeeded)
        {
          userManager.AddToRole(admin.Id, role1.Name);
          userManager.AddToRole(admin.Id, role2.Name);
          userManager.AddToRole(user1.Id, role2.Name);
          userManager.AddToRole(user2.Id, role2.Name);
        }

        var book1 = new Article
        {
          ApplicationUserId = user1.Id,
          Description =
            "Согласно последним прогнозам, компания Huawei, известный производитель бытовой электроники, столкнётся с резким сокращением своей доли на мировом рынке смартфонов в 2021 году.\r\n\r\nКак утверждает BusinessStandard, из-за американских санкций китайский производитель в этом году займёт лишь 14% мирового рынка смартфонов, а в следующему году аналитики отдают компании всего 4%. Ранее на этой неделе исследователи TrendForce заявили, что продолжающиеся ограничения со стороны правительства США станут основной причиной такого значительного снижения показателей Huawei.",
          Name = "Huawei пугают забвением. Компании отдают всего 4% рынка смартфонов в 2021 году"
        };
        var book2 = new Article
        {
          ApplicationUserId = admin.Id,
          Description =
            "Пока это только дизайнерский проект\r\nНесмотря на общий успех электромобилей Tesla в Европе, большие автомобили вроде Model S и Model X, на самом деле, не так уж и часто встречаются на европейских дорогах. Глава компании Илон Маск (Elon Musk) сделал интересное заявление, проясняющее, как Tesla собирается в дальнейшем завоёвывать Европу.",
          Name = "Илон Маск рассказал о компактном электромобиле Tesla для Европы"
          //Price = 110
        };
        var book3 = new Article
        {
          ApplicationUserId = user2.Id,
          Description =
            "Microsoft уже отметила «лучший запуск Xbox» в истории, но Фил Спенсер отметил, что, независимо от игр на старте новых консолей, обе корпорации распродали все устройства и теперь борются за то, чтобы как можно скорее выпустить на рынок новые.\r\n\r\nОднако оказывается, что Xbox Series X|S может быть даже меньше на рынке, чем PlayStation 5. Глава бренда Xbox сообщил в недавнем интервью, что Microsoft начала производить консоли очень поздно из-за новой технологии AMD:\r\n\r\n«Мы начали производство в конце лета. Позже, чем конкуренты, так как ждали конкретной технологии AMD в нашем чипе. Мы немного отстали от Sony в плане создания устройств».\r\n\r\nКомпания ждала полную реализацию технологии AMD RDNA 2, но инженеры пока не подтвердили, какие преимущества благодаря этому получат новые консоли и почему это так важно.\r\n\r\n",
          Name = "Xbox Series X|S меньше на рынке, чем PS5, поскольку Microsoft ждала полной реализации RDNA 2"
        };
        var book4 = new Article
        {
          ApplicationUserId = user2.Id,
          Description =
            "LEGO представила новые наборы линейки Art — на этот раз они посвящёны Гарри Поттеру. Всего есть четыре герба факультетов Хогвартса. Покупатель наборов сможет собрать четыре небольшие картинки с гербами Гриффиндора, Слизерина, Когтеврана и Пуффендуя. При желании их получится объединить в один большой холст с буквой «H» посередине.\n Набор по Гарри Поттеру в продаже с первого января 2021 года. Стоимость составит 120 долларов (~8800 рублей). Ранее LEGO выпустила похожие наборы по вселенным «Звёздных войн», «Железного человека», а также портреты Мэрилин Монро и участников группы Beatles.",
        Name = "LEGO представила наборы линейки Art, посвящённые Гарри Поттеру"
        };

        var form1 = new Form
        {
          Comment = "Сайт просто супер, моя любимая секция - Главная!",
          Section = (Section) 1
        };
        
        var review1 = new Review
        {
          ApplicationUserId = user1.Id,
          Comment = "Эххх, китайцы китайцы...",
          ArticleId = 1,
          Rate = Rate.Awesome,
          DateCreation = DateTime.Now
        };
        var review2 = new Review
        {
          ApplicationUserId = user1.Id,
          Comment = "Huawei ЖИВИ!",
          ArticleId = 1,
          Rate = Rate.Good,
          DateCreation = DateTime.Now
        };
        var review3 = new Review
                {
                  ApplicationUserId = user1.Id,
                  Comment = "Илон - гений современности",
                  ArticleId = 2,
                  Rate = Rate.Good,
                  DateCreation = DateTime.Now
                };
        var review4 = new Review
                {
                  ApplicationUserId = user1.Id,
                  Comment = "Всегда знал, что Маск - не гений,\n а просто маркетолог",
                  ArticleId = 2,
                  Rate = Rate.Good,
                  DateCreation = DateTime.Now
                };
var review5 = new Review
                {
                  ApplicationUserId = user1.Id,
                  Comment = "Я - сонибой",
                  ArticleId = 3,
                  Rate = Rate.Good,
                  DateCreation = DateTime.Now
                };
var review6 = new Review
                {
                  ApplicationUserId = user1.Id,
                  Comment = "Уже затарились холодильником?",
                  ArticleId = 3,
                  Rate = Rate.Good,
                  DateCreation = DateTime.Now
                };


        var tag1 = new Tag
        {
          Name = "Научпоп"
        };
        var tag2 = new Tag
        {
          Name = "Huawei"
        };
        var tag3 = new Tag
        {
          Name = "ElonMusk"
        };
        var tag4 = new Tag
        {
          Name = "Tesla"
        };
        var tag5 = new Tag
        {
          Name = "Xbox"
        };
        var tag6 = new Tag
        {
          Name = "Microsoft"
        };
        var tag7 = new Tag
        {
          Name = "Fill Spencer"
        };
        var tag8 = new Tag
        {
          Name = "LEGO"
        };

        ArticleTag articleTag1 = new ArticleTag
        {
          ArticleId = 1,
          TagId = 1,
        };
        ArticleTag articleTag2 = new ArticleTag
        {
          ArticleId = 1,
          TagId = 2,
        };
        ArticleTag articleTag3 = new ArticleTag
        {
          ArticleId = 2,
          TagId = 3,
        };
        ArticleTag articleTag4 = new ArticleTag
        {
          ArticleId = 2,
          TagId = 4,
        };
        ArticleTag articleTag5 = new ArticleTag
        {
          ArticleId = 2,
          TagId = 1,
        };
        ArticleTag articleTag6 = new ArticleTag
        {
          ArticleId = 3,
          TagId = 5,
        };
ArticleTag articleTag7 = new ArticleTag
        {
          ArticleId = 3,
          TagId = 6,
        };
ArticleTag articleTag8 = new ArticleTag
        {
          ArticleId = 3,
          TagId = 7,
        };
ArticleTag articleTag9 = new ArticleTag
        {
          ArticleId = 4,
          TagId = 8,
        };

//User user1 = new User
        //{
        //  Age = 19,
        //  Email = "dedkabob@gmail.com",
        //  Name = "Nikita",
        //  Password = "Nikita"
        //};

        //var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
        //var roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(context));



        context.Articles.Add(book1);
        context.Articles.Add(book2);
        context.Articles.Add(book3);
        context.Articles.Add(book4);

        context.Reviews.Add(review1);
        context.Reviews.Add(review2);
        context.Reviews.Add(review3);
        context.Reviews.Add(review4);
        context.Reviews.Add(review5);
        context.Reviews.Add(review6);

        //context.Forms.Add(form1);


        context.Tags.Add(tag1);
        context.Tags.Add(tag2);
        context.Tags.Add(tag3);
        context.Tags.Add(tag4);
        context.Tags.Add(tag5);
        context.Tags.Add(tag6);
        context.Tags.Add(tag7);
        context.Tags.Add(tag8);

        ///IMPORTANT
        context.SaveChanges();

        context.ArticleTags.Add(articleTag1);
        context.ArticleTags.Add(articleTag2);
        context.ArticleTags.Add(articleTag3);
        context.ArticleTags.Add(articleTag4);
        context.ArticleTags.Add(articleTag5);
        context.ArticleTags.Add(articleTag6);
        context.ArticleTags.Add(articleTag7);
        context.ArticleTags.Add(articleTag8);
        context.ArticleTags.Add(articleTag9);
        //context.ArticleTags.Add(articleTag4);
        //context.ArticleTags.Add(articleTag5);
        //context.ArticleTags.Add(articleTag6);

        //context.Users.Add(user1);


        context.SaveChanges();

        base.Seed(context);
      }
      catch (DbEntityValidationException e)
      {
        foreach (var eve in e.EntityValidationErrors)
        {
          Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            eve.Entry.Entity.GetType().Name, eve.Entry.State);
          foreach (var ve in eve.ValidationErrors)
            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
              ve.PropertyName, ve.ErrorMessage);
        }

        //throw;
      }
    }
  }
}