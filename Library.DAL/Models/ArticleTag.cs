﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Library.DAL.Models
{
  public class ArticleTag
  {
    [Key] 
    public int Id { get; set; }
    //// Два первичных/внешних ключа в промежуточной таблице ArticleTag
    //[Key, ForeignKey("Article")] 
    //// Sorting order
    //[Column("ArticleId", Order = 0)]
    public int ArticleId { get; set; }

    //[Key, ForeignKey("Tag")] 
    //[Column(Order = 1)]
    public int TagId { get; set; }

    //public string Name { get; set; }

    public virtual Article Article { get; set; }
    public virtual Tag Tag { get; set; }

    //public ArticleTag()
    //{
    //  Article = new Article();
    //  Tag = new Tag();
    //}
  }
}