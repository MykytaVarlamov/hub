﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.DAL.Models 
{
  /// <summary>
  /// Класс для управления аутентификацией, предоставления прав
  /// </summary>
  public class ApplicationUser : IdentityUser
  {
    public virtual ClientProfile ClientProfile { get; set; }
    public ICollection<Article> Articles { get; set; }
    public ICollection<Review> Reviews { get; set; }
    //public ICollection<UserSubscriptions> UserSubscriptionses { get; set; }
  }
}