﻿//using Library.

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static Library.DAL.Enum;

namespace Library.DAL.Models
{
  public class Review
  {
    [Key]
    public int Id { get; set; }

    [Required]
    public string Comment { get; set; }

    public Rate Rate { get; set; }

    [Required]
    public DateTime DateCreation { get; set; }

    [Required]
    [ForeignKey("Article")]
    [Column("Article_ArticleId")]
    public int ArticleId { get; set; }

    [ForeignKey("ApplicationUser")]
    public string ApplicationUserId { get; set; }

    public virtual Article Article { get; set; }
    public virtual ApplicationUser ApplicationUser { get; set; }

    public Review()
    {
      //ApplicationUser = new ApplicationUser();
    }

  }
}