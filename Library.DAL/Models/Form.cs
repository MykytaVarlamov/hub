﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using static Library.DAL.Enum;

namespace Library.DAL.Models
{
  public class Form
  {
    [Key]
    [DisplayName("Id заявки")]
    public int Id { get; set; }

    public string Email { get; set; }

    [Display(Name = "Раздел")]
    [Required(ErrorMessage = "Раздел должен быть выбран")]
    public Section Section { get; set; }

    [Display(Name = "Комментарий")]
    [Required]
    public string Comment { get; set; }

  }
}