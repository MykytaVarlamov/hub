﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Library.DAL.Models
{
  public class UserSubscriptions
  {
    [Key] 
    public int Id { get; set; }

    [Required]
    public string ApplicationUser_Id1 { get; set; }
    [ForeignKey("ApplicationUser_Id1")]
    public ApplicationUser ApplicationUser { get; set; }

    [Required]
    public string ApplicationUser_Id2 { get; set; }
    [ForeignKey("ApplicationUser_Id2")]
    public ApplicationUser User { get; set; }

}
}