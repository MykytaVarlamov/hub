﻿using System.ComponentModel.DataAnnotations;

namespace Library.DAL.Models
{
  /// <summary>
  /// Модель пользователя
  /// <list type="table">
  /// <item>
  /// <term>Id</term>
  /// <description>
  /// </description>
  /// <term>Name</term>
  /// <description>
  /// </description>
  /// </item><item>
  /// <term>Email</term>
  /// <description>
  /// </description>
  /// </item><item>
  /// <term>Pasword</term>
  /// <description>
  /// </description>
  /// </item><item>
  /// <term>Age</term>
  /// <description>
  /// </description>
  /// </item><item>
  /// </list>
  /// </summary>
  public class User
  {
    // Id
    /// <value>Id пользователя</value>
    [Key]
    public int Id { get; set; }
    /// <value>Имя пользователя</value>
    public string Name { get; set; }
    /// <value>Email пользователя</value>
    public string Email { get; set; }
    /// <value>Пароль пользователя</value>
    public string Password { get; set; }
    /// <value>Возраст пользователя</value>
    public int Age { get; set; }


    /////<value>Список отзывов пользователя</value>   
    //public ICollection<Review> Reviews { get; set; }

    /////<value>Список Форм пользователя</value>   
    //public ICollection<Form> Forms { get; set; }
    ///// <value>Конструтор по умолчанию</value>
    //public User()
    //{
    //  Reviews = new List<Review>();
    //  Forms = new List<Form>();
    //}
  }
}