﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.DAL.Models
{
  public class Article
  {
    /// ID of Article
    [Key]
    public int Id { get; set; }

    /// Name of article
    [Required]
    public string Name { get; set; }

    /// <summary>
    /// Description of article
    /// </summary>
    [Required]
    public string Description { get; set; }

    [Required]
    [ForeignKey("ApplicationUser")]
    public string ApplicationUserId { get; set; }
    public virtual ApplicationUser ApplicationUser { get; set; }


    // Список тегов относящихся к статье
    public virtual ICollection<ArticleTag> ArticleTags { get; set; }

    // Список комментариев относящихся к этой статье
    public virtual ICollection<Review> Reviews { get; set; }

    public Article()
    {
      //ApplicationUser = new ApplicationUser();
      Reviews = new List<Review>();
      ArticleTags = new List<ArticleTag>();
    }

  }

}