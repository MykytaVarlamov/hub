﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.DAL.Models
{
  /// <summary>
  ///Класс для хранения данных пользователяю Связан 1 к 1
  /// с классом ApplicationUser
  /// данные, то есть профиль пользователя, которые
  /// не играют никакой роли при аутентификации.
  /// </summary>
  public class ClientProfile
  {
    [Key]
    [ForeignKey("ApplicationUser")]
    public string Id { get; set; }

    public string Name { get; set; }
    public string Address { get; set; }
    /// <summary>
    /// По этому нав. свойству АппЮзер подтянет профиль
    /// </summary>
    
    [Required]
    public virtual ApplicationUser ApplicationUser { get; set; }
  }
}