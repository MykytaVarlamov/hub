﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library.DAL.Models
{
  public class Tag
  {
    [Key] 
    public int Id { get; set; }

    [Required]
    [StringLength(100, ErrorMessage = "Максимум 100 символов", MinimumLength = 3)]
    public string Name { get; set; }

    // If needed, will load all articles. 
    public virtual ICollection<ArticleTag> ArticleTags { get; set; }

  }
}