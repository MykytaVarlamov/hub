﻿using Library.DAL.EF;
using Library.DAL.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Library.DAL.Models;
using System.Linq;

namespace Library.DAL.Repositories
{
  public class ArticleRepository : IRepository<Article>
  {
    private LibContext db;

    public ArticleRepository(LibContext context)
    {
      this.db = context;
    }
    // АСИНХРОННО НЕ РАБОТАЛО 
    public void CreateAsyncAsTask(Article item)
    {
      db.Articles.Add(item);
    }

    public async Task<IEnumerable<Article>> GetAllAsyncAsTask()
    {
      return await Task.Run(() => db.Articles);
    }

    //public Article GetById(int id)
    //{
    //  return db.Articles.Include(a=>a.ArticleTags).FirstOrDefault(a=> a.Id == id);
    //}
    public async Task<Article> GetAsyncAsTask(int id)
    {
      return await Task.Run(() => db.Articles.Include(a => a.ArticleTags).FirstOrDefaultAsync(a => a.Id == id));
    }

    public async void DeleteAsync(int id)
    {
      Article book = await db.Articles.FindAsync(id);
      if (book != null)
      {
        db.Articles.Remove(book);
      }
    }
    

    public void Update(Article item)
    {
      db.Entry(item).State = EntityState.Modified;
    }

    public IEnumerable<Article> GetAll()
    {
      return db.Articles;
    }
  }
}