﻿using Library.DAL.EF;
using Library.DAL.Interfaces;
using Library.DAL.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Library.DAL.Repositories
{
  public class FormRepository : IRepository<Form>
  {
    //private LibContext db = new LibContext();
    private LibContext db;
    public FormRepository(LibContext context)
    {
      this.db = context;
    }
    public void Create(Form item)
    {
      db.Forms.Add(item);
    }


    public void CreateAsyncAsTask(Form item)
    {
       Task.Run(()=>db.Forms.Add(item));
    }

    public async Task<IEnumerable<Form>> GetAllAsyncAsTask()
    {
      return await Task.Run(()=>db.Forms);
    }

    public async Task<List<Form>> GetAllAsyncAsList()
    {
      return await Task.Run(() => db.Forms).Result.ToListAsync();
    }

    public async Task<Form> GetAsyncAsTask(int id)
    {
      return await Task.Run(()=>db.Forms.Find(id));

    }

    public async void DeleteAsync(int id)
    {
      Form form = await db.Forms.FindAsync(id);
      if (form != null)
      {
        db.Forms.Remove(form);
      }
    }

    public void Update(Form item)
    {
      db.Entry(item).State = EntityState.Modified;
    }

    public IEnumerable<Form> GetAll()
    {
      throw new System.NotImplementedException();
    }

    public Form GetById(int id)
    {
      throw new System.NotImplementedException();
    }
  }
}