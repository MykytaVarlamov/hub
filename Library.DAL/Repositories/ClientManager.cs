﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.DAL.EF;
using Library.DAL.Interfaces;
using Library.DAL.Models;

namespace Library.DAL.Repositories
{
  public class ClientManager : IClientManager
  {
    public LibContext Database { get; set; }

    public ClientManager(LibContext db)
    {
      Database = db;
    }
    public void Dispose()
    {
      Database.Dispose();
    }

    public void Create(ClientProfile item)
    {
      Database.ClientProfiles.Add(item);
      Database.SaveChanges();
    }
  }
}