﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Library.DAL.EF;
using Library.DAL.Interfaces;
using Library.DAL.Models;

namespace Library.DAL.Repositories
{
  public class TagRepository : IRepository<Tag>, ITagRepository
  {

    private LibContext db;

    public TagRepository(LibContext context)
    {
      this.db = context;
    }

    public async void DeleteAsync(int id)
    {
      Tag tag = await db.Tags.FindAsync(id);
      if (tag != null)
      {
        db.Tags.Remove(tag);
      }
    }

    public async Task<Tag> GetAsyncAsTask(int id)
    {
      return await db.Tags.FindAsync(id);
    }

    public async Task<IEnumerable<Tag>> GetAllAsyncAsTask()
    {
      return await Task.Run(()=>db.Tags);
    }
public IEnumerable<Tag> GetAll()
    {
      return db.Tags;
    }

    public void Update(Tag item)
    {
      db.Entry(item).State = EntityState.Modified;
    }


    public void CreateAsyncAsTask(Tag item)
    {
      db.Tags.Add(item);
    }

    public Tag GetById(int id)
    {
      throw new NotImplementedException();
    }
  }
}