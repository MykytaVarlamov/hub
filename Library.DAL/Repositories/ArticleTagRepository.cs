﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Library.DAL.EF;
using Library.DAL.Interfaces;
using Library.DAL.Models;

namespace Library.DAL.Repositories
{
  public class ArticleTagRepository : IRepository<ArticleTag>
  {
    private LibContext db;
    public ArticleTagRepository(LibContext context)
    {
      this.db = context;
    }
    public void CreateAsyncAsTask(ArticleTag item)
    {
      db.ArticleTags.Add(item);
    }

    public Task<IEnumerable<ArticleTag>> GetAllAsyncAsTask()
    {
      throw new NotImplementedException();
    }

    public Task<ArticleTag> GetAsyncAsTask(int id)
    {
      throw new NotImplementedException();
    }

    public void DeleteAsync(int id)
    {
      throw new NotImplementedException();
    }

    public void Update(ArticleTag item)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<ArticleTag> GetAll()
    {
      throw new NotImplementedException();
    }
  }
}