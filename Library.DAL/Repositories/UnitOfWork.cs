﻿using Library.DAL.EF;
using Library.DAL.Interfaces;
using Library.DAL.Models;
using System;
using Library.DAL.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Threading.Tasks;

namespace Library.DAL.Repositories
{
  public class UnitOfWork : IUnitOfWork
  {
    private LibContext db;

    private ArticleRepository articleRepository;
    private FormRepository formRepository;
    private ReviewRepository reviewRepository;
    public TagRepository tagRepository { get; set; }
    public ArticleTagRepository articleTagRepository { get; set; }

    private ApplicationUserManager userManager;
    private ApplicationRoleManager roleManager;
    private IClientManager clientManager;
    public UnitOfWork(string connectionString)
    {
      db = new LibContext(connectionString);
      userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
      roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(db));
      clientManager = new ClientManager(db);
    }

    public IRepository<Article> Articles
    {
      get
      {
        if (articleRepository == null)
        {
          articleRepository = new ArticleRepository(db);
        }
        return articleRepository;
      }
    }

    public IRepository<Form> Forms
    {
      get
      {
        if (formRepository == null)
        {
          formRepository = new FormRepository(db);
        }
        return formRepository;
      }
    }


    public IRepository<Review> Reviews
    {
      get
      {
        if (reviewRepository == null)
        {
          reviewRepository = new ReviewRepository(db);
        }
        return reviewRepository;
      }
    }
    
    public IRepository<Tag> Tags
        {
          get
          {
            if (tagRepository == null)
            {
              tagRepository = new TagRepository(db);
            }
            return tagRepository;
          }
        }

    public IRepository<ArticleTag> ArticleTags
    {
      get
      {
        if (articleTagRepository == null)
        {
          articleTagRepository = new ArticleTagRepository(db);
        }

        return articleTagRepository;
      }
    }
    public ApplicationUserManager UserManager
    {
      get
      {
        return userManager;
      }
    }

    public ApplicationRoleManager RoleManager
    {
      get
      {
        return roleManager;
      }
    }

    public IClientManager ClientManager
    {
      get
      {
        return clientManager;
      }
    }

    public async Task SaveAsync()
    {
      await db.SaveChangesAsync();
    }

    public void Save()
    {
      db.SaveChanges();
    }

    private bool disposed = false;
    public virtual void Dispose(bool disposing)
    {
      if (!this.disposed)
      {
        if (disposing)
        {
          userManager.Dispose();
          roleManager.Dispose();
          clientManager.Dispose();
          db.Dispose();
        }
        this.disposed = true;
      }
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }
  }
}