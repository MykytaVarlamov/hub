﻿using Library.DAL.EF;
using Library.DAL.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;
using Library.DAL.Models;

namespace Library.DAL.Repositories
{
  public class ReviewRepository : IRepository<Review>
  {
    //private LibContext db = new LibContext();
    private LibContext db;
    public ReviewRepository(LibContext context)
    {
      this.db = context;
    }

    public async Task<IEnumerable<Review>> GetAllAsyncAsTask()
    {
      return await Task.Run(() => db.Reviews.Include(r => r.Article));
    }

    public async Task<Review> GetAsyncAsTask(int id)
    {
      return await Task.Run(()=>db.Reviews.Find(id));
    }

    public void DeleteAsync(int id)
    {
      Review review = db.Reviews.Find(id);
      if (review != null)
      {
        db.Reviews.Remove(review);
      };
    }

    public void Update(Review item)
    {
      db.Entry(item).State = EntityState.Modified;
    }

    public IEnumerable<Review> GetAll()
    {
      return db.Reviews;
    }

    public void CreateAsyncAsTask(Review item)
    {
      db.Reviews.Add(item);
      //Thread.Sleep(3000);
    }

    public Review GetById(int id)
    {
      throw new System.NotImplementedException();
    }
  }
}