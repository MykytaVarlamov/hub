﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.DAL.Models;
using Microsoft.AspNet.Identity;

namespace Library.DAL.Identity
{
  /// <summary>
  /// Данный класс будет управлять ролями
  /// По сути эти классы выполняют роль репозиториев.
  /// </summary>
  public class ApplicationRoleManager : RoleManager<ApplicationRole>
  {
    public ApplicationRoleManager(IRoleStore<ApplicationRole, string> store) : base(store)
    {

    }
  }
}