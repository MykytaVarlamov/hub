﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.DAL.Models;
using Microsoft.AspNet.Identity;

namespace Library.DAL.Identity
{
  /// <summary>
  /// Данный класс будет управлять пользователями:
  /// добавлять их в базу данных и аутентифицировать.
  /// По сути эти классы выполняют роль репозиториев.
  /// </summary>
  public class ApplicationUserManager : UserManager<ApplicationUser>
  {
    public ApplicationUserManager(IUserStore<ApplicationUser> store) : base(store)
    {
    }
  }
}