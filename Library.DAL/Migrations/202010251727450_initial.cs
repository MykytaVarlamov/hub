﻿namespace Library.DAL.Migrations
{
  using System.Data.Entity.Migrations;

  public partial class initial : DbMigration
  {
    public override void Up()
    {
      CreateTable(
          "dbo.Books",
          c => new
          {
            Id = c.Int(nullable: false, identity: true),
            Name = c.String(),
            Author = c.String(),
            Description = c.String(),
            Price = c.Int(nullable: false),
          })
          .PrimaryKey(t => t.Id);

      CreateTable(
          "dbo.Reviews",
          c => new
          {
            Id = c.Int(nullable: false, identity: true),
            Comment = c.String(nullable: false),
            Rate = c.Int(nullable: false),
            Book_BookId = c.Int(nullable: false),
          })
          .PrimaryKey(t => t.Id)
          .ForeignKey("dbo.Books", t => t.Book_BookId, cascadeDelete: true)
          .Index(t => t.Book_BookId);

      CreateTable(
          "dbo.Forms",
          c => new
          {
            Id = c.Int(nullable: false, identity: true),
            Section = c.Int(nullable: false),
            Comment = c.String(nullable: false),
          })
          .PrimaryKey(t => t.Id);

      CreateTable(
          "dbo.Users",
          c => new
          {
            Id = c.Int(nullable: false, identity: true),
            Name = c.String(),
            Email = c.String(),
            Password = c.String(),
            Age = c.Int(nullable: false),
          })
          .PrimaryKey(t => t.Id);

    }

    public override void Down()
    {
      DropForeignKey("dbo.Reviews", "Book_BookId", "dbo.Books");
      DropIndex("dbo.Reviews", new[] { "Book_BookId" });
      DropTable("dbo.Users");
      DropTable("dbo.Forms");
      DropTable("dbo.Reviews");
      DropTable("dbo.Books");
    }
  }
}
