﻿namespace Library.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FormEmail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Forms", "Email", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Forms", "Email");
        }
    }
}
