﻿namespace Library.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userToArticlesNew1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Articles", "ApplicationUserId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Articles", "ApplicationUserId");
            AddForeignKey("dbo.Articles", "ApplicationUserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Articles", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.Articles", new[] { "ApplicationUserId" });
            AlterColumn("dbo.Articles", "ApplicationUserId", c => c.String());
        }
    }
}
