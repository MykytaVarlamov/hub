﻿namespace Library.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ArticleTagFix3PKId : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.ArticleTags");
            AddColumn("dbo.ArticleTags", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.ArticleTags", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.ArticleTags");
            DropColumn("dbo.ArticleTags", "Id");
            AddPrimaryKey("dbo.ArticleTags", new[] { "ArticleId", "TagId" });
        }
    }
}
