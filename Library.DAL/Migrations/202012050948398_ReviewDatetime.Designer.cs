﻿// <auto-generated />
namespace Library.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class ReviewDatetime : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ReviewDatetime));
        
        string IMigrationMetadata.Id
        {
            get { return "202012050948398_ReviewDatetime"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
