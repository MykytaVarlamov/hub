﻿namespace Library.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ArticleTagFix2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Articles", "Tag_Id", "dbo.Tags");
            DropIndex("dbo.Articles", new[] { "Tag_Id" });
            DropColumn("dbo.Articles", "Tag_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Articles", "Tag_Id", c => c.Int());
            CreateIndex("dbo.Articles", "Tag_Id");
            AddForeignKey("dbo.Articles", "Tag_Id", "dbo.Tags", "Id");
        }
    }
}
