﻿namespace Library.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserIdPropToArt1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Articles", "ApplicationUserId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Articles", "ApplicationUserId");
        }
    }
}
