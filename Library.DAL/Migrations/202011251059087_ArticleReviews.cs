﻿namespace Library.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ArticleReviews : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reviews", "Article_ArticleId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Reviews", "Article_ArticleId");
        }
    }
}
