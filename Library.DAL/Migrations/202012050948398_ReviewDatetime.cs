﻿namespace Library.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReviewDatetime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reviews", "DateCreation", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Reviews", "DateCreation");
        }
    }
}
