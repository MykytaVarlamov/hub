﻿// <auto-generated />
namespace Library.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class ArticleTagFix3PKId : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ArticleTagFix3PKId));
        
        string IMigrationMetadata.Id
        {
            get { return "202011280955292_ArticleTagFix3PKId"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
