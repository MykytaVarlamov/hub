﻿namespace Library.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteArticleTagManyToMany : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TagArticles", "Tag_Id", "dbo.Tags");
            DropForeignKey("dbo.TagArticles", "Article_Id", "dbo.Articles");
            DropIndex("dbo.TagArticles", new[] { "Tag_Id" });
            DropIndex("dbo.TagArticles", new[] { "Article_Id" });
            DropTable("dbo.Tags");
            DropTable("dbo.TagArticles");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TagArticles",
                c => new
                    {
                        Tag_Id = c.Int(nullable: false),
                        Article_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_Id, t.Article_Id });
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.TagArticles", "Article_Id");
            CreateIndex("dbo.TagArticles", "Tag_Id");
            AddForeignKey("dbo.TagArticles", "Article_Id", "dbo.Articles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.TagArticles", "Tag_Id", "dbo.Tags", "Id", cascadeDelete: true);
        }
    }
}
