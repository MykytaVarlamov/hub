﻿namespace Library.DAL
{
  public static class Enum
  {
    public enum Section
    {
      Home,
      Guest,
      Form
    }
    public enum Rate
    {
      Good,
      Bad,
      VeryGood,
      Perfect,
      Awesome
    }
  }
}