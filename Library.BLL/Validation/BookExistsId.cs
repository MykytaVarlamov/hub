﻿using Library.DAL.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Library.BLL.Validation
{
  /// <summary>
  /// Validates if book with selected id exists
  /// </summary>
  public class BookExistsId : ValidationAttribute
  {
    delegate bool GreaterZero(int x);
    private IUnitOfWork db;
    public BookExistsId(IUnitOfWork uow)
    {
      db = uow;
    }
    //private UnitOfWork db = new UnitOfWork();

    public override bool IsValid(object value)
    {
      int BookId;

      bool isInt = Int32.TryParse(Convert.ToString(value),
        out BookId);
      GreaterZero IsGraterZero = (x) => x > 0;
      bool isGreaterZero = IsGraterZero(BookId);
      return ((db.Articles.GetAsyncAsTask(BookId) != null) && isInt && isGreaterZero);
    }
  }
}