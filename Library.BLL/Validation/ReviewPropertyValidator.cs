﻿using Library.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Library.BLL.Validation
{
  public class ReviewPropertyValidator : ModelValidator
  {
    public ReviewPropertyValidator(ModelMetadata metadata, ControllerContext context) : base(metadata, context)
    { }

    public override IEnumerable<ModelValidationResult> Validate(object container)
    {
      ReviewDTO r = container as ReviewDTO;
      if (r != null)
      {
        switch (Metadata.PropertyName)
        {
          case "Comment":
            if ((String.IsNullOrEmpty(r.Comment)))
            {
              return new ModelValidationResult[]
              {
                new ModelValidationResult{MemberName="Comment", Message="Комментарий не может быть пустым(Из провайдера валидаций Propery)"}
              };
            }
            break;
        }
      }
      return Enumerable.Empty<ModelValidationResult>();
    }
  }
}