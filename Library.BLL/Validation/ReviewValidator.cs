﻿using Library.BLL.DTO;
using System.Collections.Generic;
using System.Web.Mvc;
using static Library.DAL.Enum;

namespace Library.BLL.Validation
{
  public class ReviewValidator : ModelValidator
  {
    public ReviewValidator(ModelMetadata metadata, ControllerContext context) : base(metadata, context)
    { }

    public override IEnumerable<ModelValidationResult> Validate(object container)
    {
      ReviewDTO r = (ReviewDTO)Metadata.Model;
      List<ModelValidationResult> errors = new List<ModelValidationResult>();
      if (!System.Enum.IsDefined(typeof(Rate), r.Rate))
      {
        errors.Add(new ModelValidationResult { MemberName = "", Message = "Такой модели не существует (Из провайдера валидаций Model)" });
      }
      return errors;
    }

  }
}
