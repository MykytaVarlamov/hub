﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Library.DAL.Interfaces;
using Library.DAL.Repositories;

namespace Library.BLL.Validation
{
  public class CommentExists : ValidationAttribute
  {
    private IUnitOfWork Database;

    public CommentExists(IUnitOfWork unitOfWork)
    {
      Database = unitOfWork;
    }

    public override bool IsValid(object value)
    {
      string comment = value.ToString();
      return !Database.Reviews.GetAll().Any(r => r.Comment == comment);
    }
  }
}