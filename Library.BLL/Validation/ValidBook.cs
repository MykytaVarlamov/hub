﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Library.BLL.Validation
{
  public class ValidBook : ValidationAttribute
  {
    delegate bool GreaterZero(int x);

    public override bool IsValid(object value)
    {
      int BookId;
      bool isInt = Int32.TryParse(Convert.ToString(value),
        out BookId);
      GreaterZero IsGraterZero = (x) => x > 0;
      bool isGreaterZero = IsGraterZero(BookId);
      return (isInt && isGreaterZero);
    }
  }
}