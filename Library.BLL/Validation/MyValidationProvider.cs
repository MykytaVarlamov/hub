﻿using Library.BLL.DTO;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Library.BLL.Validation
{
  public class MyValidationProvider : ModelValidatorProvider
  {
    public override IEnumerable<ModelValidator> GetValidators(ModelMetadata metadata, ControllerContext context)
    {
      if (metadata.ContainerType == typeof(ReviewDTO))
      {
        return new ModelValidator[] { new ReviewPropertyValidator(metadata, context) };
      }

      if (metadata.ModelType == typeof(ReviewDTO))
      {
        return new ModelValidator[] { new ReviewValidator(metadata, context) };
      }

      return Enumerable.Empty<ModelValidator>();
    }
  }
}