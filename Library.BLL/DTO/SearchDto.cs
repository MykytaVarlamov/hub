﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.BLL.DTO
{
  public class SearchDto
  {
    public List<string> SearchTagIds { get; set; }
    public string SearchQuery { get; set; }
  }
}