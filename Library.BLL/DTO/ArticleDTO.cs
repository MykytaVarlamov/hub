﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Library.DAL.Models;

namespace Library.BLL.DTO
{
  public class ArticleDTO
  {
    // ID книги
    public int Id { get; set; }

    // Name of Article
    [Remote("IsArticleExists", "Home", HttpMethod = "POST", ErrorMessage = "Such an Article allready exists (JSON Result)")]
    public string Name { get; set; }

    [DisplayName("Описание")]
    public string Description { get; set; }

    [Required]
    public string ApplicationUserId { get; set; }
    public virtual ApplicationUser ApplicationUser { get; set; }


    public virtual ICollection<ArticleTag> ArticleTags { get; set; }
    public string[] TagIds { get; set; }
    public ICollection<Tag> NewTags { get; set; }


    // Список комментариев относящихся к этой книге
    public ICollection<Review> Reviews { get; set; }

    public ArticleDTO()
    {
      Reviews = new List<Review>();
      ArticleTags = new List<ArticleTag>();
      //ApplicationUser = new ApplicationUser();
    }
  }
}