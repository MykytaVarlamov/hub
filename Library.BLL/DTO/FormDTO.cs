﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using static Library.DAL.Enum;

namespace Library.BLL.DTO
{
  public class FormDTO
  {
    [DisplayName("Id заявки")]
    public int Id { get; set; }

    [Display(Name = "Раздел")]
    [Required(ErrorMessage = "Раздел должен быть выбран")]
    public Section Section { get; set; }

    [Display(Name = "Комментарий")]
    [Remote("IsCommentExists", "Form", HttpMethod = "POST", ErrorMessage = "Такой комментарий уже существует (JSON Result)")]
    public string Comment { get; set; }

    [RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", 
    ErrorMessage = "Enter valid email adress")]
    public string Email { get; set; }
    [Required]
    public bool isConfirmed { get; set; }
  }
}