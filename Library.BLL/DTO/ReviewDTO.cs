﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Library.DAL.Models;
using static Library.DAL.Enum;

namespace Library.BLL.DTO
{
  public class ReviewDTO : IValidatableObject
  {
    [Key]
    [DisplayName("Id отзыва")]
    public int Id { get; set; }

    //[MaxLength(20)]
    [Required(ErrorMessage = "Введите комментарий (метадата)")]
    public string Comment { get; set; }
    public Rate Rate { get; set; }
    public DateTime DateCreation { get; set; }

    //[ForeignKey("User")]
    //[Column("Rewiev_ReviewId", Order = 0)]
    //public int UserId { get; set; }
    [Required]
    //[ValidBook(ErrorMessage = "Введите цифры (Из метаданных)")]
    [ForeignKey("Article")]
    [Column("Article_ArticleId")]
    [DisplayName("Id of Article")]
    public int ArticleId { get; set; }

    public virtual Article Article { get; set; }

    public string ApplicationUserId { get; set; }

    public virtual ApplicationUser ApplicationUser { get; set; }

    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
      List<ValidationResult> errors = new List<ValidationResult>();
      if (string.IsNullOrEmpty(this.Comment))
      {
        errors.Add(new ValidationResult("Добавьте комментарий (из метаданных, самовалидация)"));
      }
      if (this.Comment.Length < 20 || this.Comment.Length > 200)
      {
        errors.Add(new ValidationResult("Длинна комментария долна быть от 20 до 200 символов (из метаданных, самовалидация)"));
      }
      return errors;
    }


  }
}