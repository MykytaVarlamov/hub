﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library.BLL.DTO
{
  public class TagDTO
  {
    public int Id { get; set; }
    [Remote("IsTagExists", "Home", HttpMethod = "POST", ErrorMessage = "Такой ТЕГ уже существует (JSON Result)")]
    public string Name { get; set; }
  }
}