﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace Library.BLL.Infrastructure.Helpers
{
  public static class SectionHelper
  {
    /// <summary>
    /// Creates the tripple section.
    /// </summary>
    /// <param name="html">The HTML.</param>
    /// <param name="author">The author.</param>
    /// <param name="content">The content.</param>
    /// <param name="header">The header.</param>
    /// <param name="htmlAttributes">The HTML attributes.</param>
    /// <returns></returns>
    public static MvcHtmlString CreateTrippleSection(this HtmlHelper html, string author,
        string content, string header = null, object htmlAttributes = null)
    {
      TagBuilder p = new TagBuilder("p");
      TagBuilder h2 = new TagBuilder("h2");
      if (header != null)
      {
        h2.SetInnerText(header);
        p.InnerHtml += h2.ToString();
      }
      TagBuilder h4 = new TagBuilder("h4");
      h4.SetInnerText(author);
      p.InnerHtml += h4.ToString();
      TagBuilder h6 = new TagBuilder("h6");
      h6.SetInnerText(content);
      p.InnerHtml += h6.ToString();
      p.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
      return MvcHtmlString.Create(p.ToString());
    }
    /// <summary>
    /// Creates the section.
    /// </summary>
    /// <param name="html">The HTML.</param>
    /// <param name="author">The author.</param>
    /// <param name="content">The content.</param>
    /// <param name="header">The header.</param>
    /// <param name="htmlAttributes">The HTML attributes.</param>
    /// <returns></returns>
    public static MvcHtmlString CreateSection(this HtmlHelper html, string author,
        string content, string header = null, object htmlAttributes = null)
    {
      string fill = "";
      TagBuilder h2 = new TagBuilder("h2");
      if (header != null)
      {
        h2.SetInnerText(header);
        fill += h2.ToString();
      }
      TagBuilder h4 = new TagBuilder("h4");
      h4.SetInnerText(author);
      fill += h4.ToString();
      TagBuilder h6 = new TagBuilder("h6");
      h6.SetInnerText(content);
      fill += h6.ToString();
      //p.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
      string output = String.Format($"<li>{fill}</li>");
      return new MvcHtmlString(output);
    }

    /// <summary>
    /// Models the display.
    /// </summary>
    /// <param name="html">The HTML.</param>
    /// <param name="obj">The object.</param>
    /// <returns>MvcHtmlString</returns>
    /// <remarks>Builds li element oh H3`s which contains each property of model or obj</remarks>
    public static MvcHtmlString ModelDisplay(this HtmlHelper html, Object obj)
    {
      string fill = "";
      string[] types = new[] {"String", "int", "Rate"};
      var last = obj.GetType().GetProperties().Where(p => !p.GetGetMethod().GetParameters().Any()).Last();
      foreach (var prop in obj.GetType().GetProperties().Where(p => !p.GetGetMethod().GetParameters().Any()))
      {
        //if (prop.Equals(last))
        //{
        //  break;
        //}
        TagBuilder h4 = new TagBuilder("h4");
        TagBuilder span1 = new TagBuilder("span");
        TagBuilder span2 = new TagBuilder("span");
        //h4.InnerHtml+= (span2.InnerHtml += prop.Name);
        
        if (types.Contains(prop.PropertyType.Name))
        {
          span1.InnerHtml += prop.Name;
          span2.InnerHtml += prop.GetValue(obj, null);
          string PropVal = $"<p>{span1}{span2}</p>";
          fill += PropVal;
        }
        
      }
      string builder = $"<li class=\"block\">{fill}</li>";
      return new MvcHtmlString(builder);
    }
  }
}
