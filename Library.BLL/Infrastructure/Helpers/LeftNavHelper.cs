﻿using System.Web.Mvc;

namespace Library.BLL.Infrastructure.Helpers
{
  public static class LeftNavHelper
  {
    public static MvcHtmlString CreateMenu(this HtmlHelper html, string[] items)
    {
      TagBuilder ul = new TagBuilder("ul");
      ul.Attributes.Add("id", "top-menu");
      for (int i = 0; i < items.Length; i++)
      {

        TagBuilder li = new TagBuilder("li");
        if (i == 0)
        {
          li.Attributes.Add("class", "active");
        }

        TagBuilder a = new TagBuilder("a");
        TagBuilder span = new TagBuilder("span");
        span.InnerHtml = items[i];
        a.InnerHtml = span.ToString();
        li.InnerHtml = a.ToString();
        ul.InnerHtml += li.ToString();
      }
      return new MvcHtmlString(ul.ToString());
    }
  }
}