﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Library.WEB.Models;

namespace Library.BLL.Infrastructure.Helpers
{
  public static class PagingHelper
  {
    /// <summary>
    /// This helper will simply create a block of links
    /// and also add classes to them for visualization.
    /// </summary>
    /// <param name="html"></param>
    /// <param name="pageInfo"></param>
    /// <param name="pageUrl"></param>
    /// <returns></returns>
    public static MvcHtmlString PageLinks(this HtmlHelper html,
      PageInfo pageInfo, Func<int, string> pageUrl)
    {
      StringBuilder result = new StringBuilder();

      for (int i = 1; i <= pageInfo.TotalPages; i++)
      {
        TagBuilder tag = new TagBuilder("a");
        tag.MergeAttribute("href", pageUrl(i));
        tag.InnerHtml = i.ToString();

        // если текущая страница, то выделяем ее,
        // например, добавляя класс
        if (i == pageInfo.PageNumber)
        {
          tag.AddCssClass("selected");
          tag.AddCssClass("btn-warning");
        }
        tag.AddCssClass("btn btn-default");
        result.Append(tag.ToString());
      }
      return MvcHtmlString.Create(result.ToString());
    }
  }
}