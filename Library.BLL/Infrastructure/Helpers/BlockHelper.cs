﻿using System.Web.Mvc;

namespace Library.BLL.Infrastructure.Helpers
{
  public static class BlockHelper
  {
    public static MvcHtmlString Image(this HtmlHelper html, string src, string alt)
    {
      TagBuilder img = new TagBuilder("img");
      img.MergeAttribute("src", src);
      img.MergeAttribute("alt", alt);

      return MvcHtmlString.Create(img.ToString(TagRenderMode.SelfClosing));
    }
  }
}