﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Library.BLL.Infrastructure.Helpers
{
  public static class NewsSection
  {
    public static MvcHtmlString TagBuilder(this HtmlHelper html, string tagName, 
      object HtmlAttributes = null)
    {
      //TagBuilder html, "div", new Dictionary<string, string>() { { "class", "hashtag" } }
      TagBuilder hashtag = new TagBuilder("div");
      hashtag.SetInnerText(tagName);
      string[] arr = new string[6]
      {
        "label label-default", "label label-primary",
        "label label-success", "label label-info",
        "label label-warning",
        "label label-danger"
      };
      Random rnd = new Random();
      hashtag.MergeAttributes(new Dictionary<string, string>() {{ "class", $"{arr[rnd.Next(0,5)]} hashtag" } });
      return new MvcHtmlString(hashtag.ToString());
    }
  }
}