﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Library.BLL.DTO;
using Library.BLL.Infrastructure;
using Library.DAL.Models;

namespace Library.BLL.Interfaces
{
  /// <summary>
  /// Через объекты данного интерфейса уровень представления будет взаимодействовать с уровнем доступа к данным. Здесь определены только три метода:
  /// Create (создание пользователей),
  /// Authenticate (аутентификация пользователей) и
  /// SetInitialData (установка начальных данных в БД - админа и списка ролей).
  /// </summary>
  public interface IUserService : IDisposable
  {
    Task<OperationDetails> Create(UserDTO userDto);
    Task<ClaimsIdentity> Authenticate(UserDTO userDto);
    Task SetInitialData(UserDTO adminDto, List<string> roles);
    Task<ApplicationUser> FindByNameAsync(string Email);

    Task<IList<string>> GetRolesAsync(string userId);
  }
}
