﻿using Library.BLL.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Library.BLL.Interfaces
{
  public interface IReviewService
  {
    void AddReview(ReviewDTO reviewDto);
    ArticleDTO GetBook(int? id);
    IEnumerable<ReviewDTO> GetReviews();
    Task<IEnumerable<ReviewDTO>> GetReviewsAsync();
    IEnumerable<SelectListItem> GetSelectListItems();
    Task<IEnumerable<ReviewDTO>> GetReviewsByDateAsyncAsTask(int sortOrder);
    void DeleteReview(int Id);
    void EditReview(int Id);
    void Dispose();
  }
}
