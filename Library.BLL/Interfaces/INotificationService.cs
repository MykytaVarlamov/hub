﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.Interfaces
{
  public interface INotificationService
  {
    void SendEmail(string email, string subject, string message);
  }
}
