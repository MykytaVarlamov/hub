﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.BLL.DTO;

namespace Library.BLL.Interfaces
{
  public interface ITagService
  {
    Task<IEnumerable<TagDTO>> GetTagsSorted(int? id);
    Task<TagDTO> GetTagByIdAsTask(int id);
    void EditTag(TagDTO tagDto);
  }
}
