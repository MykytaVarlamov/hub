﻿using System.Threading.Tasks;
using Library.BLL.DTO;

namespace Library.BLL.Interfaces
{
  public interface IFormService
  {
    Task<bool> SearchComment(string comment);
    void AddForm(FormDTO formDto);
    void Dispose();
  }
}
