﻿using Library.BLL.DTO;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Library.DAL.Models;

namespace Library.BLL.Interfaces
{
  /// <summary>
  /// Article Service implementation
  /// </summary>
  public interface IArticleService
  {
    /// <summary>
    /// Gets all article DTOs
    /// </summary>
    /// <returns>Task<IEnumerable<ArticleDTO>></returns>
    Task<IEnumerable<ArticleDTO>> GetArticleDTOs();

    /// <summary>
    /// Gets article by id
    /// </summary>
    /// <param name="Id"></param>
    /// <returns>Task&lt;ArticleDTO&gt;</returns>
    Task<ArticleDTO> GetArticleByIdAsTask(int Id);

    /// <summary>
    /// Creates an article
    /// </summary>
    /// <param name="articleDto"></param>
    void CreateArticle(ArticleDTO articleDto);

    /// <summary>
    /// Creates an articleTag
    /// </summary>
    /// <param name="articleTag"></param>
    void CreateArticleTag(ArticleTag articleTag);

    /// <summary>
    /// Creates a comment
    /// </summary>
    /// <param name="reviewDto"></param>
    void CreateComment(ReviewDTO reviewDto);

    /// <summary>
    /// Creates a tag
    /// </summary>
    /// <param name="tagDto"></param>
    void CreateTag(TagDTO tagDto);

    /// <summary>
    /// Get all articles filtered and sorted
    /// </summary>
    /// <param name="searchDto"></param>
    /// <returns>Task<IEnumerable<ArticleDTO>></returns>
    Task<IEnumerable<ArticleDTO>> GetArticlesBySearch(SearchDto searchDto);

    /// <summary>
    /// Checks wether this Tag allready exists
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    bool IsTagExists(string name);

    /// <summary>
    /// Checks wether this Article allready exists
    /// </summary>
    /// <param name="name"></param>
    /// <returns>bool</returns>
    bool IsArticleExists(string name);

    /// <summary>
    /// Checks wether this comment allready exists
    /// </summary>
    /// <param name="name"></param>
    /// <returns>bool</returns>
    Task<bool> IsCommentExists (string name);

    /// <summary>
    /// Gets all tags
    /// </summary>
    /// <returns>Task<IEnumerable<Tag>></returns>
    Task<IEnumerable<Tag>> GetAllTagsAsync();

    /// <summary>
    /// Disposes unused data
    /// </summary>
    void Dispose();
  }
}
