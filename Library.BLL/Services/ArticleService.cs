﻿using System;
using AutoMapper;
using Library.BLL.DTO;
using Library.BLL.Interfaces;
using Library.DAL.Interfaces;
using Library.DAL.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Library.BLL.Infrastructure;
using Library.DAL.Models;

namespace Library.BLL.Services
{
  public class ArticleService : IArticleService
  {
    private IUnitOfWork Database { get; set; }

    public ArticleService(IUnitOfWork uow)
    {
      Database = uow;
    }
    IMapper mapper = new MapperConfiguration(cfg => cfg.CreateMap<Article, ArticleDTO>()).CreateMapper();

    //private UnitOfWork Database = new UnitOfWork();

    public async Task<IEnumerable<ArticleDTO>> GetArticleDTOs()
    {
      return mapper.Map<IEnumerable<Article>, IEnumerable<ArticleDTO>>
        (await Database.Articles.GetAllAsyncAsTask()); 
    }

    //public ArticleDTO GetArticleById (int Id)
    //{
    //  return mapper.Map<Article, ArticleDTO>(Database.Articles.GetById(Id));
    //}

    public async Task<ArticleDTO> GetArticleByIdAsTask(int Id)
    {
      //await Database.Articles.GetAsyncAsTask(Id);
      return mapper.Map<Article, ArticleDTO>(await Database.Articles.GetAsyncAsTask(Id));
    }
    
    // ЗДЕСЬ АСИНХРОННОСТЬ ЛОМАЕТ
    public void CreateArticle(ArticleDTO articleDto)
    {
      /*IEnumerable<Tag> tags = await Database.Tags.GetAllAsyncAsTask()*/;
      IEnumerable<Tag> tags =  Database.Tags.GetAll();
      if (String.IsNullOrEmpty(articleDto.Name))
      {
        throw new ValidationException("Name of article can`t be empty (service)", "");
      }

      if (String.IsNullOrEmpty(articleDto.Description))
      {
        throw new ValidationException("Description of article can`t be empty (service)", "");
      }

      if (articleDto.Name.Length < 20)
      {
        throw new ValidationException("Name of article can`t be less than 20 characters (service)", "");
      }

      if (articleDto.Description.Length < 40)
      {
        throw new ValidationException("Description of article can`t be less than 40 characters (service)", "");
      }

      List<string> newTagsList = new List<string>();
      int lastTagId = tags.LastOrDefault().Id;
      if (articleDto.NewTags != null)
      {
        foreach (Tag tag in articleDto.NewTags)
      {
        if (!tags.Select(t => t.Name.ToLower().Trim()).Contains(tag.Name.ToLower().Trim()))
        {
          try
          {
            Database.Tags.CreateAsyncAsTask(tag);
          }
          catch (ValidationException ex)
          {
            throw new ValidationException(ex.Message, ex.Property);
          }
          lastTagId++;
          newTagsList.Add(lastTagId.ToString());
        }
      }
      }
      

      newTagsList.AddRange(articleDto.TagIds);
      articleDto.TagIds = newTagsList.ToArray();

      Article article = new Article
      {
        Name = articleDto.Name,
        Description = articleDto.Description,
        ApplicationUserId = articleDto.ApplicationUserId
      };
      Database.Articles.CreateAsyncAsTask(article);

      Database.Save();


      /// Creating ArticleTag entities

      IEnumerable<Article> articles = Database.Articles.GetAll();
      int id = articles.LastOrDefault().Id;
      if (articleDto.TagIds!=null)
      {
        foreach (string createTagId in articleDto.TagIds)
        {
          //articleDto.Tags.Add(articleService.GetAllTagsAsync().Result.FirstOrDefault(t => t.Id == int.Parse(createTagId)));
          Database.ArticleTags.CreateAsyncAsTask(new ArticleTag()
          {
            ArticleId = id,
            TagId = int.Parse(createTagId)
          });
        }
      }
      Database.Save();
    }

 //public async void CreateArticle(ArticleDTO articleDto)
 //   {
 //     /*IEnumerable<Tag> tags = await Database.Tags.GetAllAsyncAsTask()*/;
 //     IEnumerable<Tag> tags =  Database.Tags.GetAll();
 //     if (String.IsNullOrEmpty(articleDto.Name))
 //     {
 //       throw new ValidationException("Name of article can`t be empty (service)", "");
 //     }

 //     if (String.IsNullOrEmpty(articleDto.Description))
 //     {
 //       throw new ValidationException("Description of article can`t be empty (service)", "");
 //     }

 //     if (articleDto.Name.Length < 20)
 //     {
 //       throw new ValidationException("Name of article can`t be less than 20 characters (service)", "");
 //     }

 //     if (articleDto.Description.Length < 40)
 //     {
 //       throw new ValidationException("Description of article can`t be less than 40 characters (service)", "");
 //     }

 //     List<string> newTagsList = new List<string>();
 //     int lastTagId = tags.LastOrDefault().Id;

 //     foreach (Tag tag in articleDto.NewTags)
 //     {
 //       if (!tags.Select(t => t.Name.ToLower().Trim()).Contains(tag.Name.ToLower().Trim()))
 //       {
 //         Database.Tags.CreateAsyncAsTask(tag);
 //         lastTagId++;
 //         newTagsList.Add(lastTagId.ToString());
 //       }
 //     }

 //     newTagsList.AddRange(articleDto.TagIds);
 //     articleDto.TagIds = newTagsList.ToArray();

 //     Article article = new Article
 //     {
 //       Name = articleDto.Name,
 //       Description = articleDto.Description
 //     };
 //     Database.Articles.CreateAsyncAsTask(article);
 //     Database.Save();


 //     /// Creating ArticleTag entities

 //     IEnumerable<Article> articles = await Database.Articles.GetAllAsyncAsTask();
 //     int id = articles.LastOrDefault().Id;
 //     if (articleDto.TagIds!=null)
 //     {
 //       foreach (string createTagId in articleDto.TagIds)
 //       {
 //         //articleDto.Tags.Add(articleService.GetAllTagsAsync().Result.FirstOrDefault(t => t.Id == int.Parse(createTagId)));
 //         Database.ArticleTags.CreateAsyncAsTask(new ArticleTag()
 //         {
 //           ArticleId = id,
 //           TagId = int.Parse(createTagId)
 //         });
 //       }
 //     }
 //     Database.Save();
 //   }

    public void CreateComment(ReviewDTO reviewDto)
    {
      int articleId;
      //if (article == null)
      //{
      //  throw new ValidationException("Article hasn`t been found (service)", "");
      //}
      if (!Int32.TryParse(Convert.ToString(reviewDto.ArticleId), out articleId))
      {
        throw new ValidationException("No article with such Id (service)", "");
      }

      if (!Enum.IsDefined(typeof(DAL.Enum.Rate), reviewDto.Rate))
      {
        throw new ValidationException("Choose rate from listed suggestions (service)", "");
      }
      if (string.IsNullOrEmpty(reviewDto.Comment))
      {
        throw new ValidationException("Comment can`t be empty (service)", "");
      }
      if (!Enumerable.Range(20, 200).Contains(reviewDto.Comment.Length))
      {
        throw new ValidationException("Enter a comment from 20 to 200 characters long (service)", "");
      }
      
      Review review = new Review
      {
        ApplicationUserId = reviewDto.ApplicationUserId,
        ArticleId = reviewDto.ArticleId,
        Comment = reviewDto.Comment,
        Rate = reviewDto.Rate,
        DateCreation = reviewDto.DateCreation
      };
      Database.Reviews.CreateAsyncAsTask(review);
      Database.Save();
    }

    public void CreateTag(TagDTO tagDto)
    {
      if (tagDto.Name.Length< 3)
      {
        throw new ValidationException("Tag name cant be less than 3", "Name");
      }

      Tag tag = new Tag
      {
        Name = tagDto.Name
      };
      Database.Tags.CreateAsyncAsTask(tag);
      Database.Save();
    }

    public void CreateArticleTag(ArticleTag articleTag)
    {
      Database.ArticleTags.CreateAsyncAsTask(articleTag);
      Database.Save();
    }


    public async Task<IEnumerable<Tag>> GetAllTagsAsync()
    {
      return await Task.Run(()=>Database.Tags.GetAllAsyncAsTask().Result);
    }

    public bool IsTagExists(string name)
    {
      return !Database.Tags.GetAllAsyncAsTask().Result.Any(t => t.Name.Equals(name));
    }

    public bool IsArticleExists(string name)
    {
      return  !Database.Articles.GetAllAsyncAsTask().Result.Any(a => a.Name.Equals(name));
    }
    

    public List<string> NormalizeString(string SearchQuery)
    {
      char[] bannedChars = new[] { '.', ',', ';', '!', '?' };
      List<string> searchQueryWords =
        SearchQuery.Split(' ').ToList();

      List<string> searchQueryWordsTrimmed = new List<string>();
      foreach (string searchQueryWord in searchQueryWords)
      {
        List<char> trimmedWord = new List<char>();
        foreach (char c in searchQueryWord)
        {
          if (!bannedChars.Any(cha => cha == c))
          {
            trimmedWord.Add(c);
          }
        }
        searchQueryWordsTrimmed.Add(new string(trimmedWord.ToArray()));
      }

      return searchQueryWordsTrimmed;
    }
    public async Task<IEnumerable<ArticleDTO>> GetArticlesBySearch(SearchDto searchDto)
    {
      if (searchDto.SearchTagIds.Count == 0 && searchDto.SearchQuery.Length == 0)
      {
        throw new ValidationException("Search query cant be empty !", "");
      }

      List<Article> allArticles = Database.Articles.GetAll().ToList();

      Task<List<string>> searchQueryWordsTrimmedTask = Task.Run(() => NormalizeString(searchDto.SearchQuery));
      List<string> searchQueryWordsTrimmed = await searchQueryWordsTrimmedTask;
      //List<string> searchQueryWordsTrimmed = NormalizeString(searchDto.SearchQuery);

      if (searchDto.SearchTagIds.Count != 0 && searchDto.SearchQuery.Length==0)
      {
        //foreach (Article article in allArticles)
        //{
        //  article.Select(a => a.ArticleTags.Where(t => searchDto.SearchTagIds.Any(tag => tag == t.TagId.ToString())))
        //}
        
        var machedTags = 
          allArticles
          .ToList()
          .Select(a => a.ArticleTags
            .Where(t => searchDto.SearchTagIds
              .Any(tag => tag == t.TagId.ToString())).Count()).ToList();
        IEnumerable<Article> ArticlesAndMatchedTagsCount = allArticles.Zip(machedTags,
            (art, tagsCount) => new
            {
              Article = art,
              MatchedCount = tagsCount
            })
          .OrderByDescending(o=>o.MatchedCount).Select(o=>o.Article);
        return mapper.Map<IEnumerable<Article>, IEnumerable<ArticleDTO>>
          (ArticlesAndMatchedTagsCount);
      }
      else if(searchDto.SearchTagIds.Count == 0 && searchDto.SearchQuery.Length != 0)
      {
        List<List<string>> splittedArticles = new List<List<string>>();
       var articleWords=  allArticles
          .ToList()
          .Select(a=>a.Name + " "+ a.Description);
       foreach (string words in articleWords)
       {
         splittedArticles.Add(NormalizeString(words));
       }
       List<int> countedOverlaps = new List<int>();

       foreach (List<string> listOfWords in splittedArticles)
       {
         int i = 0;
          i = listOfWords.Where(w => searchQueryWordsTrimmed.Any(sqw => sqw == w)).Count();
          //foreach (string word in listOfWords)
          //{
          //  if (searchQueryWordsTrimmed.Any(w => w == word))
          //  {
          //    i++;
          //  }
          //}
          countedOverlaps.Add(i);
       }

       IEnumerable<Article> articlesSorted = allArticles.Zip(countedOverlaps,
           (art, laps) =>
             new
             {
               Article = art,
               Overlaps = laps
             })
         .OrderByDescending(o => o.Overlaps)
         .Select(o => o.Article);
       return mapper.Map<IEnumerable<Article>, IEnumerable<ArticleDTO>>
         (articlesSorted);
      } else if (searchDto.SearchTagIds.Count != 0 && searchDto.SearchQuery.Length != 0)
      {
        List<List<string>> splittedArticles = new List<List<string>>();
        var articleWords = allArticles
          .ToList()
          .Select(a => a.Name + " " + a.Description);
        foreach (string words in articleWords)
        {
          splittedArticles.Add(NormalizeString(words));
        }
        List<int> countedOverlapsForWords = new List<int>();

        foreach (List<string> listOfWords in splittedArticles)
        {
          int i = 0;
          i = listOfWords.Where(w => searchQueryWordsTrimmed.Any(sqw => sqw == w)).Count();
          countedOverlapsForWords.Add(i);
        }

        var machedTags =
          allArticles
            .ToList()
            .Select(a => a.ArticleTags
              .Where(t => searchDto.SearchTagIds
                .Any(tag => tag == t.TagId.ToString())).Count()).ToList();
        //var zippedCount = countedOverlapsForWords.Zip(machedTags,
        //    (tags, words) => new
        //    {
        //      overlaps = tags + words
        //    });
        List<int> counted = new List<int>();
        for (int i = 0; i < countedOverlapsForWords.Count; i++)
        {
          counted.Add(machedTags.ElementAt(i) + countedOverlapsForWords.ElementAt(i));
        }
        //foreach (int countedOverlapsForWord in countedOverlapsForWords)
        //{
        //  int i = 0;
        //  counted.Add(machedTags.ElementAt(i) + countedOverlapsForWord);
        //  i++;
        //}
          IEnumerable<Article> matchedArticles = counted.Zip(allArticles,
            (count, art) => new
            {
              Article = art,
              cnt = count
            }).OrderByDescending(o => o.cnt)
          .Select(o => o.Article);
        return mapper.Map<IEnumerable<Article>, IEnumerable<ArticleDTO>>
          (matchedArticles);
      }

      return mapper.Map<IEnumerable<Article>, IEnumerable<ArticleDTO>>
        (new List<Article>());


    }

    public async Task<bool> IsCommentExists(string name)
    {
      var allReviews = await Database.Reviews.GetAllAsyncAsTask();
      return !allReviews.Any(a => a.Comment.Equals(name));
    }

    public void Dispose()
    {
      Database.Dispose();
    }
  }
}