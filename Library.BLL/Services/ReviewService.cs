﻿using AutoMapper;
using Library.BLL.DTO;
using Library.BLL.Infrastructure;
using Library.BLL.Interfaces;
using Library.DAL.Interfaces;
using Library.DAL.Repositories;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Library.DAL.Models;


namespace Library.BLL.Services
{
  public class ReviewService : IReviewService
  {
    IUnitOfWork Database { get; set; }

    private MapperConfiguration configMapperReviews 
      = new MapperConfiguration(cfg => cfg.CreateMap<Review, ReviewDTO>());
    public ReviewService(IUnitOfWork uow)
    {
      Database = uow;
    }
    //private UnitOfWork Database = new UnitOfWork();
    public async void AddReview(ReviewDTO reviewDto)
    {
      int articleId;
      Article article = await Database.Articles.GetAsyncAsTask(reviewDto.ArticleId);
      if (article == null)
      {
        throw new ValidationException("Article hasn`t been found (service)", "");
      }
      if (!Int32.TryParse(Convert.ToString(reviewDto.ArticleId), out articleId))
      {
        throw new ValidationException("No article with such Id (service)", "");
      }

      if (string.IsNullOrEmpty(reviewDto.Comment))
      {
        throw new ValidationException("Comment can`t be empty (service)", "");
      }
      if (!Enumerable.Range(20, 200).Contains(reviewDto.Comment.Length))
      {
        throw new ValidationException("Enter a comment from 20 to 200 characters long (service)", "");
      }
      //if (reviewDto.Comment.Length == 0)
      //{
      //  throw new ValidationException("Комментарий не может быть пустым", "");
      //}
      Review review = new Review
      {
        ArticleId = article.Id,
        Comment = reviewDto.Comment,
        Rate = reviewDto.Rate
      };
      Database.Reviews.CreateAsyncAsTask(review);
      Database.Save();
    }

    public void EditReview(int Id)
    {
      throw new NotImplementedException();
    }

    public ArticleDTO GetBook(int? id)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<ReviewDTO> GetReviews()
    {
      // применяем автомаппер для проекции одной коллекции на другую
      var mapper = new Mapper(configMapperReviews);
      return mapper.Map<IEnumerable<Review>, IEnumerable<ReviewDTO>>(Database.Reviews.GetAllAsyncAsTask().Result);
    }

    public async Task<IEnumerable<ReviewDTO>> GetReviewsAsync()
    {
      var mapper = new Mapper(configMapperReviews);
      return mapper.Map<IEnumerable<Review>, IEnumerable<ReviewDTO>>(await Database.Reviews.GetAllAsyncAsTask());
    }

    public IEnumerable<SelectListItem> GetSelectListItems()
    {
      IEnumerable<SelectListItem> selectListItems = new List<SelectListItem>();
      selectListItems = (from articles in Database.Articles.GetAllAsyncAsTask().Result
                         select new SelectListItem()
                         {
                           Text = articles.Name,
                           Value = articles.Id.ToString(),
                           Selected = true
                         }).ToList();
      return selectListItems;
    }

    public async Task<IEnumerable<ReviewDTO>> GetReviewsByDateAsyncAsTask(int sortOrder)
    {
      var mapper = new Mapper(configMapperReviews);
      IEnumerable<ReviewDTO> allReviews = mapper.Map<IEnumerable<Review>, IEnumerable<ReviewDTO>>(await Database.Reviews.GetAllAsyncAsTask());
      switch (sortOrder)
      {
        case 1:
        {
          return allReviews.OrderByDescending(r => r.DateCreation);
        }
        case 0:
        {
          return allReviews.OrderBy(r => r.DateCreation);
        }
      }
      return allReviews.OrderByDescending(r => r.DateCreation);
    }

    public void DeleteReview(int Id)
    {
        Database.Reviews.DeleteAsync(Id);
        Database.SaveAsync();
    }

    public void Dispose()
    {
      Database.Dispose();
    }
  }
}