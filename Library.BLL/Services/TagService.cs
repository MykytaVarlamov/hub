﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using Library.BLL.DTO;
using Library.BLL.Interfaces;
using Library.DAL.Interfaces;
using Library.DAL.Models;

namespace Library.BLL.Services
{
  public class TagService : ITagService
  {
    private IUnitOfWork Database;

    public TagService(IUnitOfWork uow)
    {
      Database = uow;
    }

    private MapperConfiguration config = new MapperConfiguration(cfg
      =>cfg.CreateMap<Tag, TagDTO>());
    public async Task<IEnumerable<TagDTO>> GetTagsSorted(int? id)
    {
      var mapper = new Mapper(config);
      IEnumerable<TagDTO> allTags = mapper.Map<IEnumerable<TagDTO>>(await Database.Tags.GetAllAsyncAsTask());
      switch (id)
      {
        case 0:
        {
          return allTags.OrderBy(t => t.Name);
        }
        case 1:
        {
          return allTags.OrderByDescending(t => t.Name);
        }
      }
      return allTags.OrderBy(t => t.Name);
    }

    public async Task<TagDTO> GetTagByIdAsTask(int id)
    {
      var mapper = new Mapper(config);
      return mapper.Map<TagDTO>(await Database.Tags.GetAsyncAsTask(id));
    }

    public async void EditTag(TagDTO tagDto)
    {
      var mapper = new Mapper(config);
      var tag = new Tag
      {
        Id = tagDto.Id,
        Name = tagDto.Name
      };
      Database.Tags.Update(tag);
      Database.Save();
    }
  }
}