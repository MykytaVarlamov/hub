﻿using Library.BLL.DTO;
using Library.BLL.Infrastructure;
using Library.BLL.Interfaces;
using Library.DAL.Interfaces;
using Library.DAL.Models;
using Library.DAL.Repositories;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static Library.DAL.Enum;

namespace Library.BLL.Services
{
  public class FormService : IFormService
  {
    //private UnitOfWork Database = new UnitOfWork();

    private IUnitOfWork Database { get; set; }
    public FormService(IUnitOfWork uow)
    {
      Database = uow;
    }
    public void AddForm(FormDTO formDto)
    {
      if (!Enum.IsDefined(typeof(Section), formDto.Section))
      {
        throw new ValidationException("Такой секции не существует (из Сервиса)", "");
      }
      if (!formDto.isConfirmed)
      {
        throw new ValidationException("Вы должны подтвердить соглашение (Из сервиса)", "");
      }
      Form form = new Form
      {
        Comment = formDto.Comment,
        Section = formDto.Section,
        Email = formDto.Email
      };
      Database.Forms.CreateAsyncAsTask(form);
      Database.SaveAsync();
    }

    public void Dispose()
    {
      Database.Dispose();
    }

    public async Task<bool> SearchComment(string comment)
    {
      var forms = Database.Forms.GetAllAsyncAsTask();
      var formsResult = await forms;
      return !formsResult.Any(f => f.Comment.Equals(comment));
    }
  }
}