﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Library.BLL.DTO;
using Library.BLL.Infrastructure;
using Library.BLL.Interfaces;
using Library.DAL.Interfaces;
using Library.DAL.Models;
using Microsoft.AspNet.Identity;

namespace Library.BLL.Services
{
  public class UserService :IUserService
  {
    IUnitOfWork Database { get; set; }

    public UserService(IUnitOfWork uow)
    {
      Database = uow;
    }

    public async Task<OperationDetails> Create(UserDTO userDto)
    {
      ApplicationUser user = await Database.UserManager.FindByEmailAsync(userDto.Email);
      if (user == null)
      {
        user = new ApplicationUser { Email = userDto.Email, UserName = userDto.Email };
        var result = await Database.UserManager.CreateAsync(user, userDto.Password);
        if (result.Errors.Count() > 0)
          return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
        // добавляем роль
        await Database.UserManager.AddToRoleAsync(user.Id, userDto.Role);
        // создаем профиль клиента
        ClientProfile clientProfile = new ClientProfile { Id = user.Id, Address = userDto.Address, Name = userDto.Name };
        Database.ClientManager.Create(clientProfile);
        await Database.SaveAsync();
        return new OperationDetails(true, "Регистрация успешно пройдена", "");
      }
      else
      {
        return new OperationDetails(false, "Пользователь с таким логином уже существует", "Email");
      }
    }

    public async Task<ClaimsIdentity> Authenticate(UserDTO userDto)
    {
      ClaimsIdentity claim = null;
      // находим пользователя
      ApplicationUser user = await Database.UserManager.FindAsync(userDto.Email, userDto.Password);
      // авторизуем его и возвращаем объект ClaimsIdentity
      if (user != null)
        claim = await Database.UserManager.CreateIdentityAsync(user,
        DefaultAuthenticationTypes.ApplicationCookie);
      return claim;
    }

    // начальная инициализация бд
    public async Task SetInitialData(UserDTO adminDto, List<string> roles)
    {
      foreach (string roleName in roles)
      {
        var role = await Database.RoleManager.FindByNameAsync(roleName);
        if (role == null)
        {
          role = new ApplicationRole { Name = roleName };
          await Database.RoleManager.CreateAsync(role);
        }
      }
      await Create(adminDto);
    }

    public async Task<ApplicationUser> FindByNameAsync(string Name)
    {
      return await Database.UserManager.FindByNameAsync(Name);
    }

    public async Task<IList<string>> GetRolesAsync(string userId)
    {
      return await Database.UserManager.GetRolesAsync(userId);
    }

    public void Dispose()
    {
      Database.Dispose();
    }
  }
}