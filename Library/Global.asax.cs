﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Library.BLL.Infrastructure;
using Library.BLL.Validation;
using Library.WEB.App_Start;
using Library.WEB.Util;
using Ninject;
using Ninject.Modules;

namespace Library.WEB
{
  public class MvcApplication : System.Web.HttpApplication
  {
    protected void Application_Start()
    {
      ModelValidatorProviders.Providers.Add(new MyValidationProvider());
      AreaRegistration.RegisterAllAreas();
      FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
      RouteConfig.RegisterRoutes(RouteTable.Routes);
      BundleConfig.RegisterBundles(BundleTable.Bundles);

      //Внедрение зависимостей
      //NinjectModule servicesModule = new ServicesModule();
      //NinjectModule serviceModule = new ServiceModule("HubConnection");
      //StandardKernel kernel = new StandardKernel(serviceModule, servicesModule);
      // Он пыталяся подтянуть еще и стандартную валидацию, я ее отвязал, у мен есть свой провайдер
      //kernel.Unbind<ModelValidatorProvider>();
      //DependencyResolver.SetResolver(new Ninject.Web.Mvc.NinjectDependencyResolver(kernel));


      DependencyResolver.SetResolver(new Ninject.Web.Mvc.NinjectDependencyResolver(KernelHolder.Kernel));
    }
  }
}
