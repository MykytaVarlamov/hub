﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.DAL.Models;

namespace Library.WEB.Models
{
  public class SearchModel
  {
    /// <summary>
    /// Tags contains all tags from Db
    /// </summary>
    public IEnumerable<Tag> Tags { get; set; }
    /// <summary>
    /// TagIds contains choosen tags
    /// </summary>
    public string[] TagIds { get; set; }

    public string SearchQuery { get; set; }

    public SearchModel(IEnumerable<Tag> tags)
    {
      Tags = tags;
    }
  }
}