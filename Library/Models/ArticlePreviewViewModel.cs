﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Library.DAL.Models;

namespace Library.WEB.Models
{
  public class ArticlePreviewViewModel
  {
    public int Id { get; set; }
    [DisplayName("Заголовок")] 
    public string Name { get; set; }

    [DisplayName("Описание")] 
    public string Description { get; set; }

    public string Author { get; set; }
    public virtual ICollection<ArticleTag> ArticleTags { get; set; }
    public virtual ApplicationUser ApplicationUser { get; set; }

    public ArticlePreviewViewModel()
    {
      ArticleTags = new List<ArticleTag>();
      ApplicationUser = new ApplicationUser();
    }

  }
}