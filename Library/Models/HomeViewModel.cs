﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.WEB.Models
{
  public class HomeViewModel
  {
    public IEnumerable<ArticlePreviewViewModel> ArticlePreviewViewModel { get; set; }
    public SearchModel SearchModel { get; set; }
    public PageInfo PageInfo { get; set; }
  }
}