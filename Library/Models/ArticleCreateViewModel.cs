﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Library.DAL.Models;

namespace Library.WEB.Models
{
  public class ArticleCreateViewModel
  {
    public int Id { get; set; }
    public string Name { get; set; }

    public string Description { get; set; }
    /// <summary>
    /// Tags contains all tags from Db
    /// </summary>
    public IEnumerable<Tag> Tags { get; set; }
    /// <summary>
    /// TagIds contains choosen tags
    /// </summary>
    public string[] TagIds { get; set; }
    /// <summary>
    /// List for new created tags
    /// </summary>
    public ICollection<Tag> NewTags { get; set; }

  }
}