﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.WEB.Models
{
  public class ArticlePage
  {
    public ArticleViewModel ArticleViewModel { get; set; }
    public ReviewViewModel NewReviewViewModel { get; set; }
    public ICollection<ReviewViewModel> ReviewViewModels { get; set; }
  }
}