﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Enum = Library.DAL.Enum;

namespace Library.WEB.Models
{
  public class ReviewViewModel
  {
    public int Id { get; set; }
    public string Author { get; set; }
    public int ArticleId { get; set; }

    [Remote("IsCommentExists", "Home", HttpMethod = "POST", ErrorMessage = "Such a comment already exists (JSON Result)")]
    [Required(ErrorMessage = "Введите комментарий (метадата)")]
    [StringLength(200, MinimumLength = 20, ErrorMessage = "Недопустимая длина имени")]
    //[CommentExists]
    public string Comment { get; set; }
    public Enum.Rate Rate { get; set; }
    [DisplayName("Created At: ")]
    public string DateOfCreation { get; set; }
    //public string Selected { get; set; }
    public SelectList RateSelectList { get; set; }

    public ReviewViewModel()
    {
      RateSelectList = new SelectList(new string[]{"Good", "Perfect"});
    }
    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
      List<ValidationResult> errors = new List<ValidationResult>();
      if (string.IsNullOrEmpty(this.Comment))
      {
        errors.Add(new ValidationResult("Добавьте комментарий (из метаданных, самовалидация)"));
      }
      if (this.Comment.Length < 20 || this.Comment.Length > 200)
      {
        errors.Add(new ValidationResult("Длинна комментария долна быть от 20 до 200 символов (из метаданных, самовалидация)"));
      }
      return errors;
    }

  }
}