﻿using static Library.DAL.Enum;

namespace Library.WEB.Models
{
  public class FormViewModel
  {
    public int Id { get; set; }
    public Section Section { get; set; }
    public string Comment { get; set; }
    public bool isConfirmed { get; set; }
  }
}