﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Library.DAL.Models;

namespace Library.WEB.Models
{
  public class ArticleViewModel
  {
    // ID книги
    [Key]
    public int Id { get; set; }
    // название статьи
    [Required]
    [DisplayName("Заголовок")]
    public string Name { get; set; }

    [Required]
    [DisplayName("Описание")]
    public string Description { get; set; }

    public string Author { get; set; }

    //public ApplicationUser ApplicationUser { get; set; }

    //[Required]
    //public int ApplicationUserId { get; set; }

    // Список тегов относящихся к статье
    public virtual ICollection<ArticleTag> ArticleTags { get; set; }


    // Список комментариев относящихся к этой статье
    public virtual ICollection<Review> Reviews { get; set; }

    public ArticleViewModel()
    {
      Reviews = new List<Review>();
      ArticleTags = new List<ArticleTag>();
    }

  }
}