﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.WEB.Models
{
  public class TagViewModel
  {
    public int Id { get; set; }
    public string Name { get; set; }
  }
}