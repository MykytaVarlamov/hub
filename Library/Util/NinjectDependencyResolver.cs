﻿using Microsoft.Ajax.Utilities;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;

namespace Library.WEB.Util
{
  //DOING NOTHING
  public class NinjectDependencyResolver
  {
    StandardKernel _kernel;

    public NinjectDependencyResolver(StandardKernel kernel)
    {
      _kernel = kernel;
      _kernel.Unbind<ModelValidatorProvider>();
    }
  }
}