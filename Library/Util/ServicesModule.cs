﻿using Library.BLL.Interfaces;
using Library.BLL.Services;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.WEB.Util
{
  /// <summary>
  /// Binds Interfaces of services to it`s realizations. So we can use 
  /// Objects of interfaces as realizations 
  /// instead of using realizations directly
  /// </summary>
  public class ServicesModule : NinjectModule
  {
    /// <summary>
    /// Binding specific realization to its interface
    /// </summary>
    public override void Load()
    {
      Bind<IArticleService>().To<ArticleService>();
      Bind<IReviewService>().To<ReviewService>();
      Bind<IFormService>().To<FormService>();
      Bind<IUserService>().To<UserService>();
      Bind<INotificationService>().To<NotificationService>();
      Bind<IAdminService>().To<AdminService>();
      Bind<ITagService>().To<TagService>();
    }
  }
}