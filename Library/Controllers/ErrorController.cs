﻿using System.Web.Mvc;

namespace Library.WEB.Controllers
{
  public class ErrorController : Controller
  {
    public ActionResult Forbidden()
    {
      Response.StatusCode = 403;
      return View();
    }

  /// <summary>
      /// Return page for error 404
      /// </summary>
      /// <returns></returns>
    public ActionResult NotFound()
      {
        Response.StatusCode = 404;
        return View();
      }

    public ActionResult InternalServerError()
    {
      Response.StatusCode = 500;
      return View();
    }

    public ActionResult ArticleNotFound()
      {
        Response.StatusCode = 404;
        return View();
      }
    public ActionResult PageNotFound()
      {
        Response.StatusCode = 404;
        return View();
      }
    }
}