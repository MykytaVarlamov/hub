﻿using System.Collections.Generic;
using System.Web.Mvc;
using Library.BLL.DTO;
using Library.BLL.Infrastructure;
using Library.BLL.Interfaces;

namespace Library.WEB.Controllers
{
  public class GuestController : Controller
  {
    //private ReviewService reviewService = new ReviewService(); 
    /// <summary>
    /// Вот сюда система (не ниджект) подставит конскретную реалтзацию
    /// этого сервиса, который мы забиндили в global.asax
    /// Теперь если мі захотим поменять к примеру реализацию сервиса 
    /// ReviewService на NewReviewService на не прийдется везде менять
    /// ReviewService на NewReviewService, мы просто перебиндим реализацию
    /// и интерфейс. 
    /// </summary>
    private IReviewService reviewService;
    public GuestController(IReviewService serv)
    {
      reviewService = serv;
    }
    // GET: Guest
    public ActionResult Index(int id)
    {
      ReviewDTO review = new ReviewDTO { ArticleId = id };
      //ViewBag.Review = review;
      ViewBag.BookId = id;
      return View(review);
    }
    [HttpGet]
    public ActionResult CommentFormPartial(int id)
    {
      //Tuple<ReviewDTO, IEnumerable<SelectListItem>> reviewRepo = 
      //  new Tuple<ReviewDTO, IEnumerable<SelectListItem>>
      //  (new ReviewDTO(), reviewService.GetSelectListItems());

      return PartialView();
    }
    [HttpPost]
    public ActionResult CommentFormPartial(ReviewDTO review)
    {
      try
      {
        reviewService.AddReview(review);
        return RedirectToAction($"Index/{review.ArticleId}");
      }
      catch (ValidationException ex)
      {
        ModelState.AddModelError(ex.Property, ex.Message);
      }
      return View("Index", review);

      //if (ModelState.IsValid)
      //{

      //}
      //else
      //{
      //  return View(review);
      //}
      ///<remarks>
      ///получаешь форму, 
      ///добавляешь туда валидационные сообщения 
      ///и возвращаешь ее обратно
      ///</remarks>
    }

    [ChildActionOnly]
    public ActionResult CommentListPartial()
    {
      List<ReviewDTO> reviews = reviewService.GetReviews() as List<ReviewDTO>;
      return PartialView(reviews);
    }
  }
}