﻿using System;
using System.Collections;
using AutoMapper;
using Library.BLL.DTO;
using Library.BLL.Interfaces;
using Library.WEB.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Library.BLL.Infrastructure;
using Library.DAL.Models;
using Library.WEB.Filters;
using Microsoft.AspNet.Identity;

namespace Library.WEB.Controllers
{
  public class HomeController : Controller
  {
    IMapper mapper = new MapperConfiguration(cfg 
        => cfg.CreateMap<ArticleDTO, ArticlePreviewViewModel>())
      .CreateMapper();

    IArticleService articleService;

    public HomeController(IArticleService serv)
    {
      articleService = serv;
    }
    MapperConfiguration configArticleDtoToView 
      = new MapperConfiguration(cfg 
        => cfg.CreateMap<ArticleDTO, ArticleViewModel>()
      .ForMember("Author", opt
        =>opt.MapFrom(rDto=>rDto.ApplicationUser.UserName)));

    private MapperConfiguration configReviewToReviewDTO = new MapperConfiguration(cfg => cfg
      .CreateMap<Review, ReviewViewModel>()
      .ForMember("Author", opt => opt.MapFrom(rDto => rDto.ApplicationUser.UserName))
      .ForMember("Comment", opt => opt.MapFrom(rDto => rDto.Comment))
      .ForMember("Rate", opt => opt.MapFrom(rDto => rDto.Rate))
      .ForMember("DateOfCreation", opt=>opt.MapFrom(rDto
        =>rDto.DateCreation.Date.ToString("d"))));

    /// <summary>
    /// Home page with all articles
    /// </summary>
    /// <returns></returns>
    [IndexExceptionPage]
    public async Task<ActionResult> Index(int page = 1)
    {
      try
      {
        var config = new MapperConfiguration(cfg 
          => cfg.CreateMap<ArticleDTO, ArticlePreviewViewModel>()
          .ForMember("Author", opt 
            => opt.MapFrom(aDto => aDto.ApplicationUser.UserName)));
        var mapper = new Mapper(config);

        IEnumerable<ArticleDTO> articleDtos = await articleService.GetArticleDTOs();
        var articles 
          = mapper.Map<IEnumerable<ArticleDTO>, IEnumerable<ArticlePreviewViewModel>>(articleDtos);
        foreach (ArticlePreviewViewModel article in articles)
        {
          string[] words = article.Description.Trim().Split(new char[] {' '});

          article.Description = String.Join(" ", words.Take(40));
        }

        int pageSize = 3;
        decimal availablePages = Math.Floor((decimal)(articles.Count()/pageSize));
        if (!Enumerable.Range(1, (int)availablePages+1).Contains(page))
        {
          throw new IndexOutOfRangeException();
        }
        IEnumerable<ArticlePreviewViewModel> articlePreviews =
          articles.Skip((page - 1) * pageSize).Take(pageSize);
        PageInfo pageInfo = new PageInfo
        {
          PageNumber = page,
          PageSize = pageSize,
          TotalItems = articles.Count()
        };
        IEnumerable<Tag> allTags = await articleService.GetAllTagsAsync();
        HomeViewModel homeModel = new HomeViewModel
        {
          ArticlePreviewViewModel = articlePreviews,
          SearchModel = new SearchModel(allTags),
          PageInfo = pageInfo
        };
        return View("Index", homeModel);
      }
      catch (ValidationException ex)
      {
        ModelState.AddModelError(ex.Property, ex.Message);
      }
      return RedirectToAction("Index");
    }

    /// <summary>
    /// Filter for existing articles
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [IndexExceptionArticle]
    public async Task<ActionResult> Article(int id)
    {
      var config = new MapperConfiguration(cfg 
        => cfg.CreateMap<ArticleDTO, ArticleViewModel>()
        .ForMember("Author", opt 
          => opt.MapFrom(aDto => aDto.ApplicationUser.UserName)));
      Mapper mapper = new Mapper(config);
      
      ArticleViewModel article =
        mapper.Map<ArticleDTO, ArticleViewModel>(await articleService.GetArticleByIdAsTask(id));
      if (article == null)
      {
        throw new IndexOutOfRangeException();
      }

      ArticlePage model = new ArticlePage
      {
        ArticleViewModel = article,
        NewReviewViewModel = new ReviewViewModel
        {
          ArticleId = article.Id
        },
        ReviewViewModels = new Mapper(configReviewToReviewDTO).Map<ICollection<ReviewViewModel>>(article.Reviews)
      };
      return View(model);
    }

    /// <summary>
    /// Create a new article for authorized users
    /// </summary>
    /// <returns></returns>
    [Authorize]
    [HttpGet]
    public async Task<ActionResult> CreateArticle()
    {
      ArticleCreateViewModel article = new ArticleCreateViewModel();
      article.Tags = await articleService.GetAllTagsAsync();
      return View(article);
    }

    /// <summary>
    /// Create a new article for authorized users
    /// POST
    /// </summary>
    /// <returns></returns>
    [Authorize]
    [HttpPost]
    public async Task<ActionResult> CreateArticle(ArticleCreateViewModel articleCreate)
    {
      ArticleDTO articleDto = new ArticleDTO
      {
        Description = articleCreate.Description,
        Name = articleCreate.Name,
        TagIds = articleCreate.TagIds,
        NewTags = articleCreate.NewTags,
        ApplicationUserId = User.Identity.GetUserId()
      };

      try
      {
        articleService.CreateArticle(articleDto);
        return  RedirectToAction("Index");
      }
      catch (ValidationException ex)
      {
        ModelState.AddModelError(ex.Property, ex.Message);
      }
      ArticleCreateViewModel articleNew = new ArticleCreateViewModel();
      articleNew.Tags = await articleService.GetAllTagsAsync();
      return View(articleNew);
    }

    /// <summary>
    /// Create an article tag
    /// </summary>
    /// <returns></returns>
    public ActionResult CreateArticleTag()
    {
      ArticleTag articleTag = new ArticleTag();
      return View(articleTag);
    }

    /// <summary>
    /// Create comment fir athorized users
    /// POST
    /// </summary>
    /// <param name="reviewViewModel"></param>
    /// <returns></returns>
    [HttpPost]
    [Authorize]
    public async Task<ActionResult> CreateComment(ReviewViewModel reviewViewModel)
    {
      Mapper mapper = new Mapper(configArticleDtoToView);
      ArticleViewModel article =
        mapper.Map<ArticleDTO, ArticleViewModel>
          (await articleService.GetArticleByIdAsTask(reviewViewModel.ArticleId));

      ReviewDTO reviewDto = new ReviewDTO
      {
        ApplicationUserId = User.Identity.GetUserId(),
        Comment = reviewViewModel.Comment,
        Rate = reviewViewModel.Rate,
        ArticleId = reviewViewModel.ArticleId,
        DateCreation = DateTime.Now
      };
      try
      {
        articleService.CreateComment(reviewDto);
        return RedirectToAction($"Article/{reviewDto.ArticleId}");
      }
      catch (ValidationException ex)
      {
        ModelState.AddModelError(ex.Property, ex.Message);
      }
      ArticleDTO newArticleDto
        = await articleService
        .GetArticleByIdAsTask(reviewViewModel.ArticleId);
      return View("Article", new ArticlePage
      {
        ArticleViewModel =
          article,
        NewReviewViewModel = new ReviewViewModel(),
        ReviewViewModels = new Mapper(configReviewToReviewDTO)
          .Map<ICollection<ReviewViewModel>>(newArticleDto.Reviews)
      });
    }


    /// <summary>
    /// Search action
    /// Searches by words coincidence
    /// Searches by tags coincidence
    /// </summary>
    /// <param name="collection"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult> Search(FormCollection collection)
    {
      try
      {
        IEnumerable<ArticleDTO> matchedArticles 
          = await articleService.GetArticlesBySearch(new SearchDto
        {
          SearchTagIds = collection["SearchModel.TagIds"] 
                         == null ? new List<string>() 
            : collection["SearchModel.TagIds"].Split(',').ToList(),

          SearchQuery = collection["SearchQuery"].Trim()
        });

        IEnumerable<Tag> allTags = await articleService.GetAllTagsAsync();
        var config = new MapperConfiguration(cfg 
          => cfg.CreateMap<ArticleDTO, ArticlePreviewViewModel>()
          .ForMember("Author", opt 
            => opt.MapFrom(aDto => aDto.ApplicationUser.UserName)));

        var mapper = new Mapper(config);
        IEnumerable<ArticlePreviewViewModel> matchedArticlesPreview =
          mapper.Map<IEnumerable<ArticlePreviewViewModel>>(matchedArticles);

        return View("Index", new HomeViewModel
        {
          ArticlePreviewViewModel = matchedArticlesPreview,
          SearchModel = new SearchModel(allTags)
        });
      }
      catch (ValidationException ex)
      {
        IEnumerable<ArticleDTO> articleDtos = await articleService.GetArticleDTOs();
        var articles 
          = mapper.Map<IEnumerable<ArticleDTO>, IEnumerable<ArticlePreviewViewModel>>(articleDtos);

        foreach (ArticlePreviewViewModel article in articles)
        {
          string[] words = article.Description.Trim().Split(new char[] { ' ' });

          article.Description = String.Join(" ", words.Take(40));
        }

        IEnumerable<Tag> Tags = await articleService.GetAllTagsAsync();
        HomeViewModel homeModel = new HomeViewModel
        {
          ArticlePreviewViewModel = articles,
          SearchModel = new SearchModel(Tags)
        };
        return View("Index", homeModel);
      }

    }

    /// <summary>
    /// Quick search by clicking
    /// on tag
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<ActionResult> SearchByTagId(int id)
    {
      IEnumerable<ArticleDTO> matchedArticles 
        = await articleService.GetArticlesBySearch(new SearchDto
      {
        SearchTagIds = id == null ? new List<string>() : new List<string>(){$"{id.ToString()}"},
        SearchQuery = ""
      });

      IEnumerable<Tag> allTags = await articleService.GetAllTagsAsync();
      var config = new MapperConfiguration(cfg 
        => cfg.CreateMap<ArticleDTO, ArticlePreviewViewModel>()
        .ForMember("Author", opt
          => opt.MapFrom(aDto => aDto.ApplicationUser.UserName)));

      var mapper = new Mapper(config);
      IEnumerable<ArticlePreviewViewModel> matchedArticlesPreview =
        mapper.Map<IEnumerable<ArticlePreviewViewModel>>(matchedArticles);

      return View("Index",new HomeViewModel
      {
        ArticlePreviewViewModel = matchedArticlesPreview,
        SearchModel = new SearchModel(allTags)
      });
    }


    /// <summary>
    /// Checks if such a comment allready exists
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    [HttpPost]
    public JsonResult IsTagExists(string name)
    {
      return Json(articleService.IsTagExists(name), JsonRequestBehavior.AllowGet);
    }
    [HttpPost]
    public JsonResult IsCommentExists(string name)
    {
      return Json(articleService.IsCommentExists(name), JsonRequestBehavior.AllowGet);
    }

    /// <summary>
    /// Searches if such an Article
    /// allready exists
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public async Task<JsonResult> IsArticleExists(string name)
    {
      return await Task.Run(()=>Json(articleService.IsArticleExists(name), JsonRequestBehavior.AllowGet));
    }

    /// <summary>
    /// Called by GC
    /// </summary>
    /// <param name="disposing"></param>
    protected override void Dispose(bool disposing)
    {
      articleService.Dispose();
      base.Dispose(disposing);
    }
  }
}