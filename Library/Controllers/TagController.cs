﻿using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Library.BLL.DTO;
using Library.BLL.Interfaces;
using Library.WEB.Models;

namespace Library.WEB.Controllers
{
  public class TagController : Controller
  {
    private readonly MapperConfiguration configTag = new MapperConfiguration(cfg => cfg
      .CreateMap<TagDTO, TagViewModel>());

    private readonly ITagService tagService;

    public TagController(ITagService tagServ)
    {
      tagService = tagServ;
    }

    [Authorize(Roles = "admin")]
    public async Task<ActionResult> EditTag(int id)
    {
      var mapper = new Mapper(configTag);
      if (id == null)
        return HttpNotFound();
      var tagDTO = await tagService.GetTagByIdAsTask(id);
      if (tagDTO != null)
      {
        var tagViewModel = new TagViewModel
        {
          Id = tagDTO.Id,
          Name = tagDTO.Name
        };
        return View(tagViewModel);
      }

      return HttpNotFound();
    }

    [Authorize(Roles = "admin")]
    [HttpPost]
    public async Task<ActionResult> EditTag(TagViewModel tagViewModel)
    {
      var tagDto = new TagDTO
      {
        Id = tagViewModel.Id,
        Name = tagViewModel.Name
      };
      tagService.EditTag(tagDto);
      return RedirectToAction("Tags", "Admin");
    }
  }
}