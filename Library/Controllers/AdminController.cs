﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Library.BLL.DTO;
using Library.BLL.Interfaces;
using Library.WEB.Models;

namespace Library.WEB.Controllers
{
  [Authorize(Roles = "admin")]
  public class AdminController : Controller
  {
    private IAdminService adminService;
    private IArticleService articleService;
    private readonly IReviewService reviewService;
    private ITagService tagService;

    private readonly MapperConfiguration configReviewToReviewDTO = new MapperConfiguration(cfg => cfg
      .CreateMap<ReviewDTO, ReviewViewModel>()
      .ForMember("Author", opt => opt.MapFrom(rDto => rDto.ApplicationUser.UserName))
      .ForMember("Comment", opt => opt.MapFrom(rDto => rDto.Comment))
      .ForMember("Rate", opt => opt.MapFrom(rDto => rDto.Rate))
      .ForMember("DateOfCreation", opt => opt.MapFrom(rDto => rDto.DateCreation.Date.ToString("d"))));

    private readonly MapperConfiguration configTagToView = new MapperConfiguration(cfg => cfg
      .CreateMap<TagDTO, TagViewModel>());


    public AdminController(IAdminService adServ, IArticleService artServ, IReviewService revServ, ITagService tagServ)
    {
      adminService = adServ;
      articleService = artServ;
      reviewService = revServ;
      tagService = tagServ;
    }

    public ActionResult Index()
    {
      return View();
    }

    public async Task<ActionResult> Comments(int? id)
    {
      var mapper = new Mapper(configReviewToReviewDTO);
      IEnumerable<ReviewViewModel> reviews = mapper.Map<IEnumerable<ReviewViewModel>>(await reviewService.GetReviewsByDateAsyncAsTask(1));
      switch (id)
      {
        case 1:
        {
          break;
        }
        case 0:
        {
          reviews = mapper.Map<IEnumerable<ReviewViewModel>>(await reviewService.GetReviewsByDateAsyncAsTask(0));
          break;
        }
      }

      return View(reviews);
    }

    public async Task<ActionResult> Tags(int? id)
    {
      var mapper = new Mapper(configTagToView);
      var tags = mapper.Map<IEnumerable<TagViewModel>>(await tagService.GetTagsSorted(id));
      return View(tags);
    }

  }
}