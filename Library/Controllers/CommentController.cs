﻿using System;
using System.Web.Mvc;
using Library.BLL.Interfaces;

namespace Library.WEB.Controllers
{
    public class CommentController : Controller
    {
      private IReviewService reviewService;

      public CommentController(IReviewService revServ)
      {
        reviewService = revServ;
      }

      [Authorize(Roles = "admin")]
      public ActionResult Delete(int id)
      {
        try
        {
          reviewService.DeleteReview(id);
          return RedirectToAction("Comments", "Admin");
        }
        catch (Exception e)
        {
          Console.WriteLine(e);
          throw;
        }
        return RedirectToAction("Comments", "Admin");
      }
  }
}