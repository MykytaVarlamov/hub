﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Library.BLL.DTO;
using Library.BLL.Infrastructure;
using Library.BLL.Interfaces;
using Library.DAL.Models;
using Library.WEB.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace Library.WEB.Controllers
{
  public class AccountController : Controller
  {
    /// <summary>
    /// Поскольку ранее мы зарегитрировали сервис пользователей через контекст OWIN,
    /// то теперь мы можем получить этот сервис с помощью метода
    /// </summary>
    private IUserService UserService
    {
      get
      {
        return HttpContext.GetOwinContext().GetUserManager<IUserService>();
      }
    }

    private IAuthenticationManager AuthenticationManager
    {
      get
      {
        return HttpContext.GetOwinContext().Authentication;
      }
    }

    public ActionResult Login()
    {
      LoginModel loginModel = new LoginModel();
      return View(loginModel);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Login(LoginModel model)
    {
      //await SetInitialDataAsync();
      if (ModelState.IsValid)
      {
        UserDTO userDto = new UserDTO { Email = model.Email, Password = model.Password };

        ///получаем объект ClaimsIdentity,
        /// который затем используется для создания
        /// аутентификационного тикета, сохраняемого в куках.
        ClaimsIdentity claim = await UserService.Authenticate(userDto);
        if (claim == null)
        {
          ModelState.AddModelError("", "Неверный логин или пароль.");
        }
        else
        {
          AuthenticationManager.SignOut();
          AuthenticationManager.SignIn(new AuthenticationProperties
          {
            IsPersistent = true
          }, claim);
          return RedirectToAction("Index", "Home");
        }
      }
      return View(model);
    }

    public ActionResult Logout()
    {
      AuthenticationManager.SignOut();
      return RedirectToAction("Index", "Home");
    }

    public ActionResult Register()
    {
      RegisterModel registerModel = new RegisterModel();
      return View(registerModel);
    }

    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Register(RegisterModel model)
    {
      //await SetInitialDataAsync();
      if (ModelState.IsValid)
      {
        UserDTO userDto = new UserDTO
        {
          Email = model.Email,
          Password = model.Password,
          Address = model.Address,
          Name = model.Name,
          Role = "user"
        };
        OperationDetails operationDetails = await UserService.Create(userDto);
        if (operationDetails.Succedeed)
          return View("RegisterSuccess");
        else
          ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
      }
      return View(model);
    }

    [Authorize]
    public async Task<ActionResult> Index()
    {
      IList<string> roles = new List<string>{"role is not defined"};
      ApplicationUser user = await UserService.FindByNameAsync(User.Identity.Name);
      if (user!=null)
      {
        roles = await UserService.GetRolesAsync(user.Id);
      }
      UserViewModel userViewModel = new UserViewModel
      {
        UserRoles = roles
      };
      return View(userViewModel);
    }

    /// <summary>
    /// Инициализирует роли и админа
    /// Не используется
    /// </summary>
    /// <returns></returns>
    private async Task SetInitialDataAsync()
    {
      await UserService.SetInitialData(new UserDTO
      {
        Email = "admin@gmail.com",
        Address = "Rybalko str. 47",
        Name = "SuperMan",
        Password = "admin",
        Role = "Admin",
        UserName = "admin"
      }, new List<string>() { "admin", "user" });

    }

  }
}
