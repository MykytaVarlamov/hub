﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using Library.BLL.DTO;
using Library.BLL.Infrastructure;
using Library.BLL.Interfaces;

namespace Library.WEB.Controllers
{
  public class FormController : Controller
  {
    //private IFormService formService = new FormService();
    private IFormService formService;
    private INotificationService notificationService;
    public FormController(IFormService serv, INotificationService not)
    {
      formService = serv;
      notificationService = not;
    }

    Dictionary<string, string> results = new Dictionary<string, string>();

    // GET
    [HttpGet]
    public ActionResult Index()
    {
      return View(new FormDTO());
    }
    [HttpPost]
    public  ActionResult Index(FormDTO _form)
    {
      try
      {
        formService.AddForm(_form);
        notificationService.SendEmail(_form.Email,"Message from Hub !", _form.Comment);
        return View("FormResult", _form);
      }
      catch (ValidationException ex)
      {
        //return Content(ex.Message);
        ModelState.AddModelError(ex.Property, ex.Message);
      }
      return View(_form);

    }

    [HttpPost]
    public async Task<JsonResult> IsCommentExists(string comment)
    {
      return Json(await formService.SearchComment(comment), JsonRequestBehavior.AllowGet);
    }

    protected override void Dispose(bool disposing)
    {
      formService.Dispose();
      base.Dispose(disposing);
    }
  }
}