﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.BLL.Infrastructure;
using Library.BLL.Interfaces;
using Library.WEB.Util;
using Ninject;
using Ninject.Modules;

namespace Library.WEB.App_Start
{
  public static class KernelHolder
  {
    private static StandardKernel kernel;

    public static StandardKernel Kernel
    {
      get
      {
        if (kernel == null)
        {
          /// Создается подключение (иньекция модулей)
          NinjectModule servicesModule = new ServicesModule();
          NinjectModule serviceModule = new ServiceModule("HubConnection");

          kernel = new StandardKernel(serviceModule, servicesModule);

          kernel.Unbind<ModelValidatorProvider>();
        }
        return kernel;
      }
    }

    public static IUserService CreateUserService()
    {
      return KernelHolder.Kernel.Get<IUserService>();
    }
  }
}