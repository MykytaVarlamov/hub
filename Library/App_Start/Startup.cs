﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.BLL.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
[assembly: OwinStartup(typeof(Library.WEB.App_Start.Startup))]

namespace Library.WEB.App_Start
{
  public class Startup
  {
    /// <summary>
    /// Для OWIN
    /// </summary>
    /// <param name="app"></param>
    public void Configuration(IAppBuilder app)
    {
      /// Передаем метод для создания ОВИНа для каждого нового пользователя сайта
      /// Потом сервис региструется контекстом OWIN:
      app.CreatePerOwinContext<IUserService>(KernelHolder.CreateUserService);
      app.UseCookieAuthentication(new CookieAuthenticationOptions
      {
        AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
        LoginPath = new PathString("/Account/Login"),
      });
    }
  }
}