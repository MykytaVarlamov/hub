﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library.WEB.Filters
{
  public class IndexExceptionPage : FilterAttribute, IExceptionFilter
  {
    public void OnException(ExceptionContext exceptionContext)
    {
      if (!exceptionContext.ExceptionHandled && exceptionContext.Exception is IndexOutOfRangeException)
      {
        exceptionContext.Result = new RedirectResult("~/Error/PageNotFound");
        exceptionContext.ExceptionHandled = true;
      }
    }
  }
}