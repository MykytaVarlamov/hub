﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library.WEB.Filters
{
  /// <summary>
  /// Exception Filter. If indexoutofrange ex
  /// is thrown, it triggers and redirects to
  /// Error/ArticleNotFound
  /// </summary>
  public class IndexExceptionArticle : FilterAttribute, IExceptionFilter
  {
    public void OnException(ExceptionContext exceptionContext)
    {
      if (exceptionContext.Exception is IndexOutOfRangeException)
      {
        exceptionContext.Result = new RedirectResult("~/Error/ArticleNotFound");
        exceptionContext.ExceptionHandled = true;
      }
    }
  }
}